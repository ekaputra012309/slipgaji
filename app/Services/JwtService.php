<?php

namespace App\Services;

class JwtService
{
    public static function generateToken(array $data)
    {
        // Get JWT configuration using the Jwt class
        $config = [
            'secret' => Jwt::$secret,
            'algorithm' => Jwt::$algorithm,
            'issuer' => Jwt::$issuer,
        ];

        // Set expiration time in the data array
        $data['exp'] = time() + Jwt::$expiration;

        // Access the JWT configuration properties
        $token = \Firebase\JWT\JWT::encode(
            $data,
            $config['secret'],
            $config['algorithm'],
            $config['issuer']
        );

        return $token;
    }
}