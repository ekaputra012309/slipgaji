<?php

namespace App\Services;

class Jwt
{
    public static $secret = 'c6b90d37f36cc72bf6bf7f869adde514c7f3a6dcca90fcd9c2c748f8a5b0d082';
    public static $algorithm = 'HS256';
    public static $issuer = 'SlipGaji';
    public static $expiration = 3600;
}
