<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class CreateJamaahTable extends Migration
{
    public function up()
    {
        $this->forge->addField([
            'id' => [
                'type' => 'INT',
                'constraint' => 5,
                'unsigned' => true,
                'auto_increment' => true,
            ],
            'nama_lengkap' => [
                'type' => 'VARCHAR',
                'constraint' => 255,
            ],
            'nomor_ktp' => [
                'type' => 'VARCHAR',
                'constraint' => 20,
            ],
            'tempat' => [
                'type' => 'VARCHAR',
                'constraint' => 255,
            ],
            'tgl_lahir' => [
                'type' => 'DATE',
            ],
            'umur' => [
                'type' => 'INT',
                'constraint' => 3,
            ],
            'kewarganegaraan' => [
                'type' => 'VARCHAR',
                'constraint' => 50,
            ],
            'alamat' => [
                'type' => 'TEXT',
                'null' => true,
            ],
            'provinsi' => [
                'type' => 'VARCHAR',
                'constraint' => 50,
            ],
            'kota' => [
                'type' => 'VARCHAR',
                'constraint' => 50,
            ],
            'kecamatan' => [
                'type' => 'VARCHAR',
                'constraint' => 50,
            ],
            'desa' => [
                'type' => 'VARCHAR',
                'constraint' => 50,
            ],
            'kode_pos' => [
                'type' => 'INT',
                'constraint' => 10,
            ],
            'handphone' => [
                'type' => 'VARCHAR',
                'constraint' => 20,
                'null' => true,
            ],
            'email' => [
                'type' => 'VARCHAR',
                'constraint' => 100,
            ],
            'pendidikan' => [
                'type' => 'VARCHAR',
                'constraint' => 50,
            ],
            'pekerjaan' => [
                'type' => 'VARCHAR',
                'constraint' => 50,
            ],
            'no_passport' => [
                'type' => 'VARCHAR',
                'constraint' => 20,
            ],
            'tgl_dikeluarkan' => [
                'type' => 'DATE',
            ],
            'tempat_dikeluarkan' => [
                'type' => 'VARCHAR',
                'constraint' => 100,
            ],
            'masa_berlaku' => [
                'type' => 'DATE',
            ],
            'nama_marham' => [
                'type' => 'VARCHAR',
                'constraint' => 100,
            ],
            'hub_marham' => [
                'type' => 'VARCHAR',
                'constraint' => 100,
            ],
            'status' => [
                'type' => 'VARCHAR',
                'constraint' => 100,
            ],
            'gol_darah' => [
                'type' => 'VARCHAR',
                'constraint' => 5,
            ],
            'baju' => [
                'type' => 'VARCHAR',
                'constraint' => 100,
            ],
            'photo' => [
                'type' => 'VARCHAR',
                'constraint' => 255,
                'null' => true,
            ],
            'created_at' => [
                'type' => 'TIMESTAMP',
                'default' => 'CURRENT_TIMESTAMP',
            ],
        ]);

        $this->forge->addKey('id', true);
        $this->forge->createTable('tbl_jamaah');
    }

    public function down()
    {
        $this->forge->dropTable('tbl_jamaah');
    }
}
