<!DOCTYPE html>
<html>

<head>
    <title><?= $title . ' ' .$payment[0]['nama_lengkap']?></title>
    <link id="custom-styles" rel="stylesheet" type="text/css" href="<?= site_url() ?>assets/css/customstyle.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
</head>

<body>
    <div class="btn-container">
        <p style="float: left; padding-left:20px"><?= $title ?></p>
        <!--<button class="btn btn-print" onclick="window.print()"><i class="fas fa-print"></i></button>-->
        <button id="print-button" class="btn btn-print"><i class="fas fa-print"></i></button>
        <div style="clear: both;"></div> <!-- Clear the floats to prevent overlap -->
    </div>

    <div class="container" id="print-container">
        <h3><?= $title ?></h3>
        <h4 style="text-align: left;">Nama Lengkap: <?= $payment[0]['nama_lengkap'] ?><span style="float: right;">Nama Paket: <?= $payment[0]['nama_paket'] ?></span></h4>

        <table>
            <thead>
                <tr>
                    <th style="width: 10px;">No</th>
                    <th>Tanggal</th>
                    <th>Keterangan</th>
                    <th colspan="2">Jumlah</th>
                    <th colspan="2">Saldo</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $no = 1;
                $sum = 0;
                foreach ($cicilan as $row) : ?>
                    <tr>
                        <td style="text-align: center;"><?= $no++ ?></td>
                        <td><?= $row['tanggal'] ?></td>
                        <td><?= $row['pilihan'] ?></td>
                        <td style="width: 15px;">Rp</td>
                        <td style="text-align: right;"><?= formatCurrency($row['nominal']) ?></td>
                        <td style="width: 15px;">Rp</td>
                        <td style="text-align: right; width:20%"><?= formatCurrency($row['saldo']) ?></td>
                    </tr>
                <?php $sum += $row['nominal'];
                endforeach; ?>
            </tbody>
        </table>
    </div>
    <div id="cetak" style="display: none;">
        <table>
            <tbody>
                <?php
                $no = 1;
                $sum = 0;
                foreach ($cicilan as $row) : ?>
                    <tr>
                        <td style="text-align: center; width: 50px;"><?= $no++ ?></td>
                        <td><?= $row['tanggal'] ?></td>
                        <td><?= $row['pilihan'] ?></td>
                        <td style="text-align: right;"><?= formatCurrency($row['nominal']) ?></td>
                        <td style="text-align: right; width:20%"><?= formatCurrency($row['saldo']) ?></td>
                    </tr>
                <?php $sum += $row['nominal'];
                endforeach; ?>
            </tbody>
        </table>
    </div>
    <script>
        document.getElementById('print-button').addEventListener('click', function () {
            // Get the content to be printed
            var contentToPrint = document.getElementById('cetak').innerHTML;
    
            // Create a new window for printing
            var printWindow = window.open('', '', 'width=1024,height=768');
            printWindow.document.open();
    
            // Add the content to the new window
            printWindow.document.write('<html><head><title>Print <?=$payment[0]['nama_lengkap']?></title>');
            // Add a style block to set table width to 100%
            printWindow.document.write('<style>table { width: 100%; }</style>');
            printWindow.document.write('</head><body>');
            printWindow.document.write(contentToPrint);
            printWindow.document.write('</body></html>');
            printWindow.document.close();
    
            // Trigger the print dialog for the new window
            printWindow.print();
            printWindow.document.close();
        });
    </script>

</body>

</html>
<?php
function formatCurrency($number)
{
    $fmt = new NumberFormatter('id-ID', NumberFormatter::DECIMAL);
    return $fmt->format($number);
}
?>