<!DOCTYPE html>
<html>

<head>
    <title>PDF Template</title>
    <style>
        /* Define your custom CSS styles here */
        body {
            font-family: Arial, sans-serif;
        }

        h1 {
            color: #333;
        }

        table {
            border-collapse: collapse;
            width: 100%;
        }

        th,
        td {
            border: 0px solid #ccc;
            padding: 8px;
            text-align: left;
        }

        th {
            background-color: #f2f2f2;
        }

        tr:nth-child(even) {
            background-color: #e0e0e0;
        }

        tr:hover {
            background-color: #d3d3d3;
        }
    </style>
</head>

<body>
    <h4 style="text-align: left;">Nama Lengkap: <?= $payment[0]['nama_lengkap'] ?><span style="float: right;">Nama Paket: <?= $payment[0]['nama_paket'] ?></span></h4>

    <table>
        <thead>
            <tr>
                <th style="width: 10px;">No</th>
                <th>Tanggal</th>
                <th>Keterangan</th>
                <th colspan="2">Jumlah</th>
                <th colspan="2">Saldo</th>
            </tr>
        </thead>
        <tbody>
            <?php
            $no = 1;
            $sum = 0;
            foreach ($cicilan as $row) : ?>
                <tr>
                    <td style="text-align: center;"><?= $no++ ?></td>
                    <td><?= $row['tanggal'] ?></td>
                    <td><?= $row['pilihan'] ?></td>
                    <td style="width: 15px;">Rp</td>
                    <td style="text-align: right;"><?= formatCurrency($row['nominal']) ?></td>
                    <td style="width: 15px;">Rp</td>
                    <td style="text-align: right;"><?= formatCurrency($row['saldo']) ?></td>
                </tr>
            <?php $sum += $row['nominal'];
            endforeach; ?>
        </tbody>
        <tfoot>
            <tr>
                <td colspan="5" style="text-align: right;">Total</td>
                <td>Rp</td>
                <td style="text-align: right;"><?= formatCurrency($sum) ?></td>
            </tr>
            <tr>
                <td colspan="5" style="text-align: right;">DP</td>
                <td>Rp</td>
                <td style="text-align: right;"><?= formatCurrency($payment[0]['dp']) ?></td>
            </tr>
            <tr style="font-size: 15pt">
                <th colspan="5" style="text-align: right;">Grand Total</th>
                <th>Rp</th>
                <th style="text-align: right;"><?= formatCurrency($payment[0]['dp']+$sum) ?></th>
            </tr>
        </tfoot>
    </table>
</body>

</html>
<?php
function formatCurrency($number)
{
    $fmt = new NumberFormatter('id-ID', NumberFormatter::DECIMAL);
    return $fmt->format($number);
}
?>