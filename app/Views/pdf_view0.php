<!DOCTYPE html>
<html>

<head>
    <title><?= $title . ' ' .$payment[0]['nama_lengkap']?></title>
    <link id="custom-styles" rel="stylesheet" type="text/css" href="<?= site_url() ?>assets/css/customstyle.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
</head>

<body>
    <div class="btn-container">
        <p style="float: left; padding-left:20px"><?= $title ?></p>
        <button class="btn btn-print" onclick="window.print()"><i class="fas fa-print"></i></button>
        <!-- <button class="btn" onclick="downloadPdf()">Download</button> -->
        <div style="clear: both;"></div> <!-- Clear the floats to prevent overlap -->
    </div>

    <div class="container" id="print-container">
        <h3><?= $title ?></h3>
        <h4 style="text-align: left;">Nama Lengkap: <?= $payment[0]['nama_lengkap'] ?><span style="float: right;">Nama Paket: <?= $payment[0]['nama_paket'] ?></span></h4>

        <table>
            <thead>
                <tr>
                    <th style="width: 10px;">No</th>
                    <th>Tanggal</th>
                    <th>Keterangan</th>
                    <th colspan="2">Jumlah</th>
                    <th colspan="2">Saldo</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $no = 1;
                $sum = 0;
                foreach ($cicilan as $row) : ?>
                    <tr>
                        <td style="text-align: center;"><?= $no++ ?></td>
                        <td><?= $row['tanggal'] ?></td>
                        <td><?= $row['pilihan'] ?></td>
                        <td style="width: 15px;">Rp</td>
                        <td style="text-align: right;"><?= formatCurrency($row['nominal']) ?></td>
                        <td style="width: 15px;">Rp</td>
                        <td style="text-align: right; width:20%"><?= formatCurrency($row['saldo']) ?></td>
                    </tr>
                <?php $sum += $row['nominal'];
                endforeach; ?>
            </tbody>
        </table>
        <!--<table>-->
        <!--    <tfoot style="border-top: 1px solid black;">-->
        <!--        <tr>-->
        <!--            <td colspan="5" style="text-align: right;">Total</td>-->
        <!--            <td>Rp</td>-->
        <!--            <td style="text-align: right;"><?= formatCurrency($sum) ?></td>-->
        <!--        </tr>-->
        <!--        <tr>-->
        <!--            <td colspan="5" style="text-align: right;">DP</td>-->
        <!--            <td>Rp</td>-->
        <!--            <td style="text-align: right;"><?= formatCurrency($payment[0]['dp']) ?></td>-->
        <!--        </tr>-->
        <!--        <tr>-->
        <!--            <th colspan="5" style="text-align: right;">Grand Total</th>-->
        <!--            <td style="width: 15px;">Rp</td>-->
        <!--            <td style="text-align: right; width: 20%"><?= formatCurrency($payment[0]['dp'] + $sum) ?></td>-->
        <!--        </tr>-->
        <!--    </tfoot>-->
        <!--</table>-->
    </div>
    <div id="cetak" style="display: none;">
        <table>
            <tbody>
                <?php
                $no = 1;
                $sum = 0;
                foreach ($cicilan as $row) : ?>
                    <tr>
                        <td style="text-align: center; : 10px;"><?= $no++ ?></td>
                        <td><?= $row['tanggal'] ?></td>
                        <td><?= $row['pilihan'] ?></td>
                        <td style="text-align: right;"><?= formatCurrency($row['nominal']) ?></td>
                        <td style="text-align: right; width:20%"><?= formatCurrency($row['saldo']) ?></td>
                    </tr>
                <?php $sum += $row['nominal'];
                endforeach; ?>
            </tbody>
        </table>
    </div>

</body>

</html>
<?php
function formatCurrency($number)
{
    $fmt = new NumberFormatter('id-ID', NumberFormatter::DECIMAL);
    return $fmt->format($number);
}
?>