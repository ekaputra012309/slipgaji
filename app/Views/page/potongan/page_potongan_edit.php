<div class="main-content">
    <section class="section">
        <div class="section-header">
            <h1><?= $title ?> </h1>
            <div class="section-header-breadcrumb">
                <div class="breadcrumb-item active"><a href="<?php echo session('status') === '1' ? site_url('dashboard') : site_url('dashboard2') ?>">Dashboard</a></div>
                <div class="breadcrumb-item"><?= $title ?></div>
            </div>
        </div>
        <div class="section-body">

            <div class="row mt-sm-4">
                <div class="col-12 col-md-12 col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <h4>Edit Data <?= $title ?></h4>
                        </div>
                        <div class="card-body">
                            <form id="formup" method="post" class="needs-validation" novalidate="">
                                <div class="row">
                                    <div class="form-group col-md-4 col-12">
                                        <label>Pegawai</label>
                                        <input type="hidden" class="form-control" name="id_potongan" id="id_potongan">
                                        <select class="form-control selectric" id="id_pegawai" name="id_pegawai" required="">
                                            <option value="">Pilih Pegawai</option>
                                        </select>
                                        <div class="invalid-feedback">
                                            Please fill in the pegawai
                                        </div>
                                    </div>
                                    <div class="form-group col-md-4 col-12">
                                        <label>Nominal Gaji</label>
                                        <input type="number" class="form-control" required="" name="gaji" id="gaji">
                                        <div class="invalid-feedback">
                                            Please fill in the gaji
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-4 col-12">
                                        <label>IKAHI CAB & DAERAH</label>
                                        <input type="number" class="form-control" required="" name="ikahi_cab" id="ikahi_cab">
                                        <div class="invalid-feedback">
                                            Please fill in the IKAHI CAB & DAERAH
                                        </div>
                                    </div>
                                    <div class="form-group col-md-4 col-12">
                                        <label>LAIN-LAIN</label>
                                        <input type="number" class="form-control" required="" name="lain2" id="lain2">
                                        <div class="invalid-feedback">
                                            Please fill in the LAIN-LAIN
                                        </div>
                                    </div>
                                    <div class="form-group col-md-4 col-12">
                                        <label>ARISAN GABUNGAN DYK</label>
                                        <input type="number" class="form-control" required="" name="arisan_gabungan" id="arisan_gabungan">
                                        <div class="invalid-feedback">
                                            Please fill in the ARISAN GABUNGAN DYK
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-4 col-12">
                                        <label>SIMPAN PINJAM</label>
                                        <input type="number" class="form-control" required="" name="simpan_pinjam" id="simpan_pinjam">
                                        <div class="invalid-feedback">
                                            Please fill in the SIMPAN PINJAM
                                        </div>
                                    </div>
                                    <div class="form-group col-md-4 col-12">
                                        <label>IURAN DYK</label>
                                        <input type="number" class="form-control" required="" name="iuran_dyk" id="iuran_dyk">
                                        <div class="invalid-feedback">
                                            Please fill in the IURAN DYK
                                        </div>
                                    </div>
                                    <div class="form-group col-md-4 col-12">
                                        <label>IURAN KOPERASI</label>
                                        <input type="number" class="form-control" required="" name="iuran_koperasi" id="iuran_koperasi">
                                        <div class="invalid-feedback">
                                            Please fill in the IURAN KOPERASI
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-4 col-12">
                                        <label>PTWP</label>
                                        <input type="number" class="form-control" required="" name="ptwp" id="ptwp">
                                        <div class="invalid-feedback">
                                            Please fill in the PTWP
                                        </div>
                                    </div>
                                    <div class="form-group col-md-4 col-12">
                                        <label>IPASPI</label>
                                        <input type="number" class="form-control" required="" name="ipaspi" id="ipaspi">
                                        <div class="invalid-feedback">
                                            Please fill in the IPASPI
                                        </div>
                                    </div>
                                    <div class="form-group col-md-4 col-12">
                                        <label>PINJAMAN KOPERASI</label>
                                        <input type="number" class="form-control" required="" name="pinjaman_koperasi" id="pinjaman_koperasi">
                                        <div class="invalid-feedback">
                                            Please fill in the PINJAMAN KOPERASI
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-4 col-12">
                                        <label>BAPOR</label>
                                        <input type="number" class="form-control" required="" name="bapor" id="bapor">
                                        <div class="invalid-feedback">
                                            Please fill in the BAPOR
                                        </div>
                                    </div>
                                    <div class="form-group col-md-4 col-12">
                                        <label>KEBERSAMAAN HAKIM</label>
                                        <input type="number" class="form-control" required="" name="kebersamaan_hakim" id="kebersamaan_hakim">
                                        <div class="invalid-feedback">
                                            Please fill in the KEBERSAMAAN HAKIM
                                        </div>
                                    </div>
                                    <div class="form-group col-md-4 col-12">
                                        <label>MUSHOLA</label>
                                        <input type="number" class="form-control" required="" name="mushola" id="mushola">
                                        <div class="invalid-feedback">
                                            Please fill in the MUSHOLA
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-4 col-12">
                                        <label>BRI/BSM/JABAR</label>
                                        <input type="number" class="form-control" required="" name="bri_bsm_jabar" id="bri_bsm_jabar">
                                        <div class="invalid-feedback">
                                            Please fill in the BRI/BSM/JABAR
                                        </div>
                                    </div>
                                    <div class="form-group col-md-4 col-12">
                                        <label>SEWA RUMAH</label>
                                        <input type="number" class="form-control" required="" name="sewa_rumah" id="sewa_rumah">
                                        <div class="invalid-feedback">
                                            Please fill in the SEWA RUMAH
                                        </div>
                                    </div>
                                    <div class="form-group col-md-4 col-12">
                                        <label>IURAN DYK HAKIM/BPDSH</label>
                                        <input type="number" class="form-control" required="" name="iuran_hakim" id="iuran_hakim">
                                        <div class="invalid-feedback">
                                            Please fill in the IURAN DYK HAKIM/BPDSH
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="form-group col-md-4 col-12">
                                        <button class="btn btn-primary">Save Changes</button>
                                        <a class="btn btn-secondary" href="<?= site_url('potongan') ?>">Back</a>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

<script>
    $(document).ready(function() {
        var urlpegawai = "<?= $url_pegawai ?>";
        var selectElement = $('#id_pegawai');
        $.ajax({
            url: urlpegawai,
            method: "GET",
            dataType: "json",
            success: function(data) {
                // Loop through the data and populate the select options
                data.data.forEach(function(item) {
                    selectElement.append($('<option>', {
                        value: item.id, // Adjust the field names as needed
                        text: item.nama_pegawai
                    }));
                });
            },
            error: function(xhr, status, error) {
                // Handle errors here
                console.log("Error: " + error);
            }
        });

        var jsonDataUrl = "<?= $url ?>";
        // Function to populate the form with JSON data
        function populateForm(data) {
            $("#id_potongan").val(data.id_potongan);
            $("#id_pegawai").val(data.id_pegawai);
            $("#gaji").val(data.gaji);
            $("#ikahi_cab").val(data.ikahi_cab);
            $("#lain2").val(data.lain2);
            $("#arisan_gabungan").val(data.arisan_gabungan);
            $("#simpan_pinjam").val(data.simpan_pinjam);
            $("#iuran_dyk").val(data.iuran_dyk);
            $("#iuran_koperasi").val(data.iuran_koperasi);
            $("#ptwp").val(data.ptwp);
            $("#ipaspi").val(data.ipaspi);
            $("#pinjaman_koperasi").val(data.pinjaman_koperasi);
            $("#bapor").val(data.bapor);
            $("#kebersamaan_hakim").val(data.kebersamaan_hakim);
            $("#mushola").val(data.mushola);
            $("#bri_bsm_jabar").val(data.bri_bsm_jabar);
            $("#sewa_rumah").val(data.sewa_rumah);
            $("#iuran_hakim").val(data.iuran_hakim);
        }

        // Fetch JSON data using AJAX
        $.ajax({
            url: jsonDataUrl,
            dataType: "json",
            success: function(data) {
                console.log(data.data);
                populateForm(data.data);
            },
            error: function() {
                console.log("Error fetching JSON data.");
            }
        });

        // add potongan
        $("#formup").submit(function(event) {
            event.preventDefault(); // Prevent the default form submission

            // Perform form validation
            var isValid = true;

            // Check each input for validity
            $("#formup input[required]").each(function() {
                if (!$(this).val()) {
                    $(this).addClass("is-invalid");
                    isValid = false;
                } else {
                    $(this).removeClass("is-invalid");
                }
            });

            if (isValid) {
                // Form is valid, proceed with submission
                var formData = new FormData(this);

                // Perform Ajax POST request
                $.ajax({
                    url: jsonDataUrl,
                    type: "POST",
                    data: formData,
                    processData: false,
                    contentType: false,
                    success: function(response) {
                        // Handle success response
                        console.log("Data updated successfully:", response);
                        if (response.messages && response.messages.success) {
                            alert(response.messages.success);
                        }
                        window.location.href = "<?= site_url('potongan') ?>";
                    },
                    error: function(error) {
                        // Handle error response
                        console.error("Error updating data:", error);
                        // Display an error message or perform other actions
                    }
                });
            }
        });

        // Add event listeners for input fields to remove validation error when potongan starts typing
        $("#formup input[required]").on("input", function() {
            $(this).removeClass("is-invalid");
        });
    });
</script>