<div class="main-content">
    <section class="section">
        <div class="section-header">
            <h1>Data <?= $title ?></h1>
            <div class="section-header-breadcrumb">
                <div class="breadcrumb-item active"><a href="<?php echo session('status') === '1' ? site_url('dashboard') : site_url('dashboard2') ?>">Dashboard</a></div>
                <div class="breadcrumb-item"><?= $title ?></div>
            </div>
        </div>

        <div class="section-body">

            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header d-flex justify-content-between align-items-center">
                            <h4 class="m-0">Data <?= $title ?></h4>
                            <?php if (session('status') !== '0') : ?>
                                <a href="<?= site_url('potongan/t') ?>" class="btn btn-primary">Add Data <i class="fas fa-plus-circle"></i></a>
                            <?php endif; ?>
                        </div>

                        <div class="card-body">
                            <div class="table-responsive">
                                <table id="example" class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th class="text-center">
                                                #
                                            </th>
                                            <th>NIP Pegawai</th>
                                            <th>Nama Pegawai</th>
                                            <th>Gaji</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

<script>
    $(document).ready(function() {
        new DataTable('#example', {
            ajax: '<?= $url ?>',
            columns: [{
                    data: null, // Custom column for action buttons
                    render: function(data, type, row) {
                        var sessionStatus = <?php echo json_encode(session('status')); ?>;
                        return '<button class="btn btn-sm btn-primary edit-button" data-id="' + row.idpotongan + '"><i class="fas fa-edit"></i></button> ' +
                            '<button class="btn btn-sm btn-danger delete-button" data-id="' + row.idpotongan + '"><i class="fas fa-trash"></i></button>';

                    },
                    className: 'text-center',
                    orderable: false // Make this column not sortable
                },
                {
                    data: 'nip'
                },
                {
                    data: 'nama_pegawai'
                },
                {
                    data: 'gaji',
                    render: function(data, type, row, meta) {
                        // Calculate the new 'gaji' value by subtracting the sum of the specified columns
                        const columnsToSubtract = [
                            'ikahi_cab',
                            'lain2',
                            'arisan_gabungan',
                            'simpan_pinjam',
                            'iuran_dyk',
                            'iuran_koperasi',
                            'ptwp',
                            'ipaspi',
                            'pinjaman_koperasi',
                            'bapor',
                            'kebersamaan_hakim',
                            'mushola',
                            'bri_bsm_jabar',
                            'sewa_rumah',
                            'iuran_hakim'
                        ];

                        const sumToSubtract = columnsToSubtract.reduce(function(acc, column) {
                            // Check if the column exists in the row data and is not null
                            if (row[column] !== null && !isNaN(row[column])) {
                                return acc + parseFloat(row[column]);
                            }
                            return acc;
                        }, 0);

                        // Calculate the new 'gaji' value
                        const newGaji = parseFloat(data) - sumToSubtract;

                        // Return the formatted 'gaji' value
                        // if (Number.isInteger(newGaji)) {
                        //     return newGaji.toFixed(0); // No decimal places
                        // } else {
                        //     return newGaji.toFixed(2); // 2 decimal places
                        // }
                        const formattedGaji = newGaji.toLocaleString('id-ID', {
                            minimumFractionDigits: 0,
                            maximumFractionDigits: 2
                        });

                        return formattedGaji;
                    },
                    className: 'dt-body-right' // Align text to the right
                },

            ],
            // Define DataTables buttons (edit and delete) event handling
            initComplete: function() {

                $('#example tbody').on('click', 'button.edit-button', function() {
                    // Handle edit button click event here
                    var id = $(this).data('id');
                    window.location.href = "<?= site_url('potongan/e/') ?>" + btoa(id);
                    // alert(id);
                });

                $('#example tbody').on('click', 'button.delete-button', function() {
                    // Handle delete button click event here
                    var id = $(this).data('id');
                    var shouldDelete = confirm("Are you sure you want to delete this potongan?");
                    if (shouldDelete) {
                        // Perform the delete Ajax request
                        $.ajax({
                            url: "<?= site_url('api/potongan/') ?>" + id, // Update with your API URL
                            type: "DELETE",
                            success: function(response) {
                                // Handle success response
                                console.log("potongan deleted successfully:", response);
                                if (response.messages && response.messages.success) {
                                    alert(response.messages.success);
                                }
                                location.reload();
                            },
                            error: function(error) {
                                // Handle error response
                                console.error("Error deleting potongan:", error);
                                // Display an error message or perform other actions
                            }
                        });
                    }
                });
            }
        });
    });
</script>