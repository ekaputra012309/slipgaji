<div class="main-content">
    <section class="section">
        <div class="section-header">
            <h1><?= $title ?> </h1>
            <div class="section-header-breadcrumb">
                <div class="breadcrumb-item active"><a href="<?php echo session('status') === '1' ? site_url('dashboard') : site_url('dashboard2') ?>">Dashboard</a></div>
                <div class="breadcrumb-item"><?= $title ?></div>
            </div>
        </div>
        <div class="section-body">

            <div class="row mt-sm-4">
                <div class="col-12 col-md-12 col-lg-12">
                    <div class="card">
                        <form id="formup" method="post" class="needs-validation" novalidate="">
                            <div class="card-header">
                                <h4>Edit Data2 <?= $title ?></h4>
                            </div>
                            <div class="card-body">
                                <div class="row">
                                    <div class="form-group col-md-4 col-12">
                                        <label>Full Name</label>
                                        <input type="text" class="form-control" name="fullname" id="fullname">
                                        <input type="hidden" class="form-control" name="id_user" id="id_user">
                                        <input type="hidden" class="form-control" name="username" id="username">
                                        <div class="invalid-feedback">
                                            Please fill in the full name
                                        </div>
                                    </div>
                                    <div class="form-group col-md-4 col-12">
                                        <label>NIP</label>
                                        <input type="text" class="form-control" required="" name="email" id="email" readonly>
                                        <div class="invalid-feedback">
                                            Please fill in the email
                                        </div>
                                    </div>
                                    <div class="form-group col-md-4 col-12">
                                        <label>Password</label>
                                        <div class="input-group">
                                            <input type="password" class="form-control" name="pass" id="pass">
                                            <input type="hidden" class="form-control" value="0" name="status" id="status">
                                            <input type="hidden" class="form-control" name="phone" id="phone">
                                            <input type="file" class="form-control d-none" id="profile_image" name="profile_image">
                                            <div class="input-group-append">
                                                <button id="togglePassword" type="button" class="btn btn-outline-secondary"><i class="fas fa-eye"></i></button>
                                            </div>
                                            <div class="invalid-feedback">
                                                Please fill in the password
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer text-right">
                                <button class="btn btn-primary">Save Changes</button>
                                <a class="btn btn-secondary" href="<?= site_url('userp') ?>">Back</a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

<script>
    $(document).ready(function() {
        // show hide password and retype password
        $('#togglePassword').click(function() {
            var passwordField = $('#pass');
            var fieldType = passwordField.attr('type');

            if (fieldType === 'password') {
                passwordField.attr('type', 'text');
                $('#togglePassword').find('i').removeClass('fa-eye').addClass('fa-eye-slash');
            } else {
                passwordField.attr('type', 'password');
                $('#togglePassword').find('i').removeClass('fa-eye-slash').addClass('fa-eye');
            }
        });

        // Assuming the JSON data URL
        var jsonDataUrl = "<?= $url ?>";
        // Function to populate the form with JSON data
        function populateForm(data) {
            $("#id_user").val(data.id);
            $("#username").val(data.username);
            $("#nama").html(data.username);
            $("#fullname").val(data.fullname);
            $("#email").val(data.email);
            $("#phone").val(data.phone);
            if (data.profile_image) {
                $("#vphoto").attr("src", "<?= site_url() ?>/" + data.profile_image);
                $("#vlama").hide();
            } else {
                $("#vphoto").hide();
                $("#vlama").show();
            }

        }

        // Fetch JSON data using AJAX
        $.ajax({
            url: jsonDataUrl,
            dataType: "json",
            success: function(data) {
                populateForm(data.data);
            },
            error: function() {
                console.log("Error fetching JSON data.");
            }
        });

        // add user
        $("#formup").submit(function(event) {
            event.preventDefault(); // Prevent the default form submission

            // Perform form validation
            var isValid = true;

            // Check each input for validity
            $("#formup input[required]").each(function() {
                if (!$(this).val()) {
                    $(this).addClass("is-invalid");
                    isValid = false;
                } else {
                    $(this).removeClass("is-invalid");
                }
            });

            if (isValid) {
                // Form is valid, proceed with submission
                var formData = new FormData(this);
                // var formDataString = 'FormData Content:\n';
                // for (var pair of formData.entries()) {
                //     formDataString += pair[0] + ': ' + pair[1] + '\n';
                // }
                // alert(formDataString);

                // Perform Ajax POST request
                $.ajax({
                    url: jsonDataUrl,
                    type: "POST",
                    data: formData,
                    processData: false,
                    contentType: false,
                    success: function(response) {
                        // Handle success response
                        console.log("Data updated successfully:", response);
                        if (response.messages && response.messages.success) {
                            alert(response.messages.success);
                        }
                        window.location.href = "<?= site_url('userp') ?>";
                    },
                    error: function(error) {
                        // Handle error response
                        console.error("Error updating data:", error);
                        // Display an error message or perform other actions
                    }
                });
            } else {
                alert('gagal update');
            }
        });

        // Add event listeners for input fields to remove validation error when user starts typing
        $("#formup input[required]").on("input", function() {
            $(this).removeClass("is-invalid");
        });
    });
</script>