<div class="main-content">
    <section class="section">
        <div class="section-header">
            <h1>Data <?= $title ?></h1>
            <div class="section-header-breadcrumb">
                <div class="breadcrumb-item active"><a href="<?php echo session('status') === '1' ? site_url('dashboard') : site_url('dashboard2') ?>">Dashboard</a></div>
                <div class="breadcrumb-item"><?= $title ?></div>
            </div>
        </div>

        <div class="section-body">

            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header d-flex justify-content-between align-items-center">
                            <h4 class="m-0">Data <?= $title ?></h4>
                            <a href="<?= site_url('user/t') ?>" class="btn btn-primary ">Add Data <i class="fas fa-plus-circle"></i></a>
                        </div>

                        <div class="card-body">
                            <div class="table-responsive">
                                <table id="example" class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th class="text-center">
                                                #
                                            </th>
                                            <th>Photo</th>
                                            <th>Username</th>
                                            <th>Nama Lengkap</th>
                                            <th>Email</th>
                                            <th>No HP</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

<script>
    $(document).ready(function() {
        new DataTable('#example', {
            ajax: '<?= $url ?>',
            columns: [{
                    data: null, // Use null for a custom column
                    render: function(data, type, row, meta) {
                        // Render the row number
                        return meta.row + 1;
                    },
                    className: 'text-center' // Center align the content
                },
                {
                    data: 'profile_image',
                    render: function(data, type, row) {
                        if (type === 'display') {
                            // Render an <img> element with the profile_image data as the src attribute
                            return '<img src="<?= site_url() ?>/' + data + '" alt="Profile Image" style="width: 50px;" class="rounded-circle">';
                        }
                        return data; // Return the raw data for other types
                    }
                },
                {
                    data: 'username'
                },
                {
                    data: 'fullname'
                },
                {
                    data: 'email'
                },
                {
                    data: 'phone'
                },
                {
                    data: null, // Custom column for action buttons
                    render: function(data, type, row) {
                        return '<button class="btn btn-sm btn-primary edit-button" data-id="' + row.id + '"><i class="fas fa-edit"></i></button> ' +
                            '<button class="btn btn-sm btn-danger delete-button" data-id="' + row.id + '"><i class="fas fa-trash"></i></button>';
                    },
                    className: 'text-center',
                    orderable: false // Make this column not sortable
                }
            ],
            // Define DataTables buttons (edit and delete) event handling
            initComplete: function() {
                $('#example tbody').on('click', 'button.edit-button', function() {
                    // Handle edit button click event here
                    var id = $(this).data('id');
                    window.location.href = "<?= site_url('user/e/') ?>" + btoa(id);
                });

                $('#example tbody').on('click', 'button.delete-button', function() {
                    // Handle delete button click event here
                    var id = $(this).data('id');
                    var shouldDelete = confirm("Are you sure you want to delete this user?");
                    if (shouldDelete) {
                        // Perform the delete Ajax request
                        $.ajax({
                            url: "<?= site_url('api/users/') ?>" + id, // Update with your API URL
                            type: "DELETE",
                            success: function(response) {
                                // Handle success response
                                console.log("User deleted successfully:", response);
                                if (response.messages && response.messages.success) {
                                    alert(response.messages.success);
                                }
                                location.reload();
                            },
                            error: function(error) {
                                // Handle error response
                                console.error("Error deleting user:", error);
                                // Display an error message or perform other actions
                            }
                        });
                    }
                });
            }
        });
    });
</script>