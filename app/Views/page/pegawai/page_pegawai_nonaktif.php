<div class="main-content">
    <section class="section">
        <div class="section-header">
            <h1>Data <?= $title ?></h1>
            <div class="section-header-breadcrumb">
                <div class="breadcrumb-item active"><a href="<?php echo session('status') === '1' ? site_url('dashboard') : site_url('dashboard2') ?>">Dashboard</a></div>
                <div class="breadcrumb-item"><?= $title ?></div>
            </div>
        </div>

        <div class="section-body">

            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header d-flex justify-content-between align-items-center">
                            <h4 class="m-0">Data <?= $title ?></h4>
                        </div>

                        <div class="card-body">
                            <div class="table-responsive">
                                <table id="example" class="table table-striped display nowrap" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th class="text-center">
                                                #
                                            </th>
                                            <th>NIP</th>
                                            <th>Nama Pegawai</th>
                                            <th>Rekening</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

<script>
    $(document).ready(function() {
        new DataTable('#example', {
            ajax: '<?= $url ?>',
            dom: 'Bfrtip',
            buttons: [
                'copy', 'csv', 'excel', 'pdf', 'print'
            ],
            columns: [{
                    data: null, // Use null for a custom column
                    render: function(data, type, row, meta) {
                        // Render the row number
                        return meta.row + 1;
                    },
                    className: 'text-center' // Center align the content
                },
                {
                    data: 'nip'
                },
                {
                    data: 'nama_pegawai'
                },
                {
                    data: 'no_rek'
                },
                {
                    data: null, // Custom column for action buttons
                    render: function(data, type, row) {
                        return '<button class="btn btn-sm btn-warning status-button" data-id="' + row.id + '" data-nama="' + row.nama_pegawai +
                            '" data-norek="' + row.no_rek + '" data-nip="' + row.nip + '">Aktif <i class="fas fa-check"></i></button>'
                    },
                    className: 'text-center',
                    orderable: false // Make this column not sortable
                }
            ],
            // Define DataTables buttons (edit and delete) event handling
            initComplete: function() {

                $('#example tbody').on('click', 'button.status-button', function() {
                    // Handle edit button click event here
                    var id = $(this).data('id');
                    var nama = $(this).data('nama');
                    var norek = $(this).data('norek');
                    var nip = $(this).data('nip');
                    var status = '1';
                    var shouldDelete = confirm("Anda yakin ingin menonaktifkan pegawai " + nama + " ?");
                    if (shouldDelete) {
                        $.ajax({
                            url: "<?= $urlS ?>/" + id,
                            type: "POST",
                            data: {
                                status: status,
                                nama_pegawai: nama,
                                no_rek: norek,
                                nip: nip
                            },
                            success: function(response) {
                                console.log("pegawai update successfully:", response);
                                if (response.messages && response.messages.success) {
                                    alert(response.messages.success);
                                }
                                location.reload();
                            },
                            error: function(error) {
                                console.error("Error deleting pegawai:", error);
                            }
                        });
                    }
                });
            }
        });
    });
</script>