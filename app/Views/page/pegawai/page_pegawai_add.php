<div class="main-content">
    <section class="section">
        <div class="section-header">
            <h1><?= $title ?> </h1>
            <div class="section-header-breadcrumb">
                <div class="breadcrumb-item active"><a href="<?php echo session('status') === '1' ? site_url('dashboard') : site_url('dashboard2') ?>">Dashboard</a></div>
                <div class="breadcrumb-item"><?= $title ?></div>
            </div>
        </div>
        <div class="section-body">

            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h4>Add Data <?= $title ?></h4>
                        </div>
                        <div class="card-body">
                            <form id="formadd" method="post" class="needs-validation" novalidate="">
                                <div class="form-group row mb-4">
                                    <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">NIP</label>
                                    <div class="col-sm-12 col-md-3">
                                        <input type="number" class="form-control" required="" name="nip" id="nip">
                                        <div class="invalid-feedback">
                                            Please fill in the NIP
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row mb-4">
                                    <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Nama Pegawai</label>
                                    <div class="col-sm-12 col-md-7">
                                        <input type="text" class="form-control" required="" name="nama_pegawai" id="nama_pegawai">
                                        <div class="invalid-feedback">
                                            Please fill in the nama pegawai
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row mb-4">
                                    <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Rekening Pegawai</label>
                                    <div class="col-sm-12 col-md-4">
                                        <input type="number" class="form-control" required="" name="no_rek" id="no_rek">
                                        <div class="invalid-feedback">
                                            Please fill in the rekening pegawai
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row mb-4">
                                    <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3"></label>
                                    <div class="col-sm-12 col-md-7">
                                        <button class="btn btn-primary">Save Changes</button>
                                        <a class="btn btn-secondary" href="<?= site_url('pegawai') ?>">Back</a>
                                    </div>
                                </div>
                            </form>
                            <form id="formuser" method="post" class="needs-validation d-none" novalidate="">
                                <input type="text" class="form-control" required="" name="username" id="username"> <br>
                                <input type="text" class="form-control" required="" name="fullname" id="fullname"> <br>
                                <input type="text" class="form-control" required="" name="email" id="email"> <br>
                                <input type="text" class="form-control" required="" name="pass" id="pass" value="12345678"> <br>
                                <input type="tel" class="form-control" name="phone" id="phone">
                                <input type="file" class="form-control" id="profile_image" name="profile_image">
                                <button class="btn btn-primary" id="btnuser">Save User</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

<script>
    $(document).ready(function() {
        // Assuming the JSON data URL
        var jsonDataUrl = "<?= $url ?>";

        // add pegawai
        $("#formadd").submit(function(event) {
            event.preventDefault(); // Prevent the default form submission

            // Perform form validation
            var isValid = true;

            // Check each input for validity
            $("#formadd input[required]").each(function() {
                if (!$(this).val()) {
                    $(this).addClass("is-invalid");
                    isValid = false;
                } else {
                    $(this).removeClass("is-invalid");
                }
            });

            if (isValid) {
                $('#btnuser').click();
                // Form is valid, proceed with submission
                var formData = new FormData(this);

                // Perform Ajax POST request
                $.ajax({
                    url: jsonDataUrl,
                    type: "POST",
                    data: formData,
                    processData: false,
                    contentType: false,
                    success: function(response) {
                        // Handle success response
                        console.log("Data updated successfully:", response);
                        if (response.messages && response.messages.success) {
                            alert(response.messages.success);
                        }
                        window.location.href = "<?= site_url('pegawai') ?>";
                    },
                    error: function(error) {
                        // Handle error response
                        console.error("Error updating data:", error);
                        // Display an error message or perform other actions
                    }
                });
            }
        });

        // Add event listeners for input fields to remove validation error when user starts typing
        $("#formadd input[required]").on("input", function() {
            $(this).removeClass("is-invalid");
        });

        $('#nip').on('input', function() {
            var inputValue = $(this).val();
            $('#email').val(inputValue);
        });
        $('#nama_pegawai').on('input', function() {
            var inputValue = $(this).val();
            $('#username').val(inputValue);
            $('#fullname').val(inputValue);
        });

        // add users
        $("#formuser").submit(function(event) {
            event.preventDefault(); // Prevent the default form submission

            // Perform form validation
            var isValid = true;

            // Check each input for validity
            $("#formuser input[required]").each(function() {
                if (!$(this).val()) {
                    $(this).addClass("is-invalid");
                    isValid = false;
                } else {
                    $(this).removeClass("is-invalid");
                }
            });

            if (isValid) {
                // Form is valid, proceed with submission
                var formData = new FormData(this);

                // Perform Ajax POST request
                $.ajax({
                    url: '<?=site_url('api/users')?>',
                    type: "POST",
                    data: formData,
                    processData: false,
                    contentType: false,
                    success: function(response) {
                        // Handle success response
                        console.log("Data user save successfully:", response);
                    },
                    error: function(error) {
                        // Handle error response
                        console.error("Error updating data:", error);
                        // Display an error message or perform other actions
                    }
                });
            }
        });
    });
</script>