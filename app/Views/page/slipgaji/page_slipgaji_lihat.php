<div class="main-content">
    <section class="section">
        <div class="section-header">
            <h1><?= $title ?> </h1>
            <div class="section-header-breadcrumb">
                <div class="breadcrumb-item active"><a href="<?php echo session('status') === '1' ? site_url('dashboard') : site_url('dashboard2') ?>">Dashboard</a></div>
                <div class="breadcrumb-item"><?= $title ?></div>
            </div>
        </div>
        <div class="section-body">

            <div class="row mt-sm-4">
                <div class="col-12 col-md-12 col-lg-12">
                    <div class="card">
                        <div class="card-header" style="display: flex; justify-content: space-between; align-items: center;">
                            <h4 style="margin: 0;">Detail Data <?= $title ?></h4>
                            <input type="hidden" id="id_slipgaji">
                            <!-- <button onclick="printTable()" class="btn btn-primary"><i class="fas fa-print"></i> Print</button> -->
                            <button class="btn btn-danger btn-download-pdf" onclick="downloadPdf()"><i class="fas fa-file-pdf"></i> Download PDF</button>
                        </div>

                        <div class="card-body">
                            <div id="print-section" style="width: auto;">
                                <div class="row justify-content-center">
                                    <img id="vlama" alt="image" src="<?= site_url() ?>assets/img/stisla-fill.svg" class="rounded-circle w-25 m-3">
                                    <img id="vphoto" alt="image" src="" class="m-3" style="width: 60px;">
                                </div>
                                <h3 style="text-align: center;">SLIP GAJI</h3>
                                <p style="text-align: center; font-weight: bolder;" class="text-uppercase" id="sliplihatnama"></p>

                                <div class="table-responsive">
                                    <table border="0">
                                        <tr>
                                            <td width="100px" style="vertical-align: top;">Pembayaran</td>
                                            <td style="vertical-align: top;"> : </td>
                                            <td colspan="2"><span id="gaji_bulan"></span></td>
                                        </tr>
                                        <tr>
                                            <td style="vertical-align: top;">Pegawai</td>
                                            <td style="vertical-align: top;"> : </td>
                                            <td colspan="2"><span id="id_pegawai"></span></td>
                                        </tr>
                                        <tr>
                                            <td style="vertical-align: top;">Rekening</td>
                                            <td style="vertical-align: top;"> : </td>
                                            <td><span id="no_rek"></span></td>
                                            <th style="width: 200px;">&nbsp;</th>
                                        </tr>
                                    </table>
                                </div>
                                <hr>
                                <div class="table-responsive">
                                    <table border="0">
                                        <tr>
                                            <th colspan="3">Gaji bersih</th>
                                            <th style="text-align: right;"><span id="gaji"></span></th>
                                            <th></th>
                                            <th style="width: 200px;">Penanggung Jawab</th>
                                        </tr>
                                        <tr>
                                            <th colspan="6">Potongan-potongan</th>
                                        </tr>
                                        <tr>
                                            <td style="vertical-align: text-top; text-align: right">1.</td>
                                            <td>IKAHI CAB & DAERAH</td>
                                            <td style="text-align: right;"><span id="ikahi_cab"></span></td>
                                            <td>&nbsp;</td>
                                            <td style="width: 20px;">&nbsp;</td>
                                            <td>Ibu Budi Hapsari</td>
                                        </tr>
                                        <tr>
                                            <td style="text-align:right">2.</td>
                                            <td>LAIN-LAIN</td>
                                            <td style="text-align: right;"><span id="lain2"></span></td>
                                            <td>&nbsp;</td>
                                            <td style="width: 20px;">&nbsp;</td>
                                            <td> - </td>
                                        </tr>
                                        <tr>
                                            <td style="text-align:right">3.</td>
                                            <td>ARISAN GABUNGAN DYK</td>
                                            <td style="text-align: right;"><span id="arisan_gabungan"></span></td>
                                            <td>&nbsp;</td>
                                            <td style="width: 20px;">&nbsp;</td>
                                            <td> - </td>
                                        </tr>
                                        <tr>
                                            <td style="text-align:right">4.</td>
                                            <td>SIMPAN PINJAM</td>
                                            <td style="text-align: right;"><span id="simpan_pinjam"></span></td>
                                            <td>&nbsp;</td>
                                            <td style="width: 20px;">&nbsp;</td>
                                            <td> - </td>
                                        </tr>
                                        <tr>
                                            <td style="text-align:right">5.</td>
                                            <td>IURAN DYK</td>
                                            <td style="text-align: right;"><span id="iuran_dyk"></span></td>
                                            <td>&nbsp;</td>
                                            <td style="width: 20px;">&nbsp;</td>
                                            <td>Ibu Watty Wiarti</td>
                                        </tr>
                                        <tr>
                                            <td style="text-align:right">6.</td>
                                            <td>IURAN KOPERASI</td>
                                            <td style="text-align: right;"><span id="iuran_koperasi"></span></td>
                                            <td>&nbsp;</td>
                                            <td style="width: 20px;">&nbsp;</td>
                                            <td>Ibu Nurhayati</td>
                                        </tr>
                                        <tr>
                                            <td style="text-align:right">7.</td>
                                            <td>PTWP</td>
                                            <td style="text-align: right;"><span id="ptwp"></span></td>
                                            <td>&nbsp;</td>
                                            <td style="width: 20px;">&nbsp;</td>
                                            <td>Ibu Inna Iskantriana</td>
                                        </tr>
                                        <tr>
                                            <td style="text-align:right">8.</td>
                                            <td>IPASPI</td>
                                            <td style="text-align: right;"><span id="ipaspi"></span></td>
                                            <td>&nbsp;</td>
                                            <td style="width: 20px;">&nbsp;</td>
                                            <td>Ibu Nurhayati</td>
                                        </tr>
                                        <tr>
                                            <td style="text-align:right">9.</td>
                                            <td>PINJAMAN KOPERASI</td>
                                            <td style="text-align: right;"><span id="pinjaman_koperasi"></span></td>
                                            <td>&nbsp;</td>
                                            <td style="width: 20px;">&nbsp;</td>
                                            <td>Ibu Nurhayati</td>
                                        </tr>
                                        <tr>
                                            <td style="text-align:right">10.</td>
                                            <td>BAPOR</td>
                                            <td style="text-align: right;"><span id="bapor"></span></td>
                                            <td>&nbsp;</td>
                                            <td style="width: 20px;">&nbsp;</td>
                                            <td>Ibu Haiva</td>
                                        </tr>
                                        <tr>
                                            <td style="text-align:right">11.</td>
                                            <td>KEBERSAMAAN HAKIM</td>
                                            <td style="text-align: right;"><span id="kebersamaan_hakim"></span></td>
                                            <td>&nbsp;</td>
                                            <td style="width: 20px;">&nbsp;</td>
                                            <td>Ibu Budi Hapsari</td>
                                        </tr>
                                        <tr>
                                            <td style="text-align:right">12.</td>
                                            <td>MUSHOLA</td>
                                            <td style="text-align: right;"><span id="mushola"></span></td>
                                            <td>&nbsp;</td>
                                            <td style="width: 20px;">&nbsp;</td>
                                            <td>Ibu Nurhayati</td>
                                        </tr>
                                        <tr>
                                            <td style="text-align:right">13.</td>
                                            <td>BRI/BSM/JABAR</td>
                                            <td style="text-align: right;"><span id="bri_bsm_jabar"></span></td>
                                            <td>&nbsp;</td>
                                            <td style="width: 20px;">&nbsp;</td>
                                            <td>Bag. Keuangan</td>
                                        </tr>
                                        <tr>
                                            <td style="text-align:right">14.</td>
                                            <td>SEWA RUMAH</td>
                                            <td style="text-align: right;"><span id="sewa_rumah"></span></td>
                                            <td>&nbsp;</td>
                                            <td style="width: 20px;">&nbsp;</td>
                                            <td>Ibu Agustina</td>
                                        </tr>
                                        <tr>
                                            <td style="text-align:right">15.</td>
                                            <td>IURAN DYK HAKIM/BPDSH</td>
                                            <td style="text-align: right;"><span id="iuran_hakim"></span></td>
                                            <td>&nbsp;</td>
                                            <td style="width: 20px;">&nbsp;</td>
                                            <td>Ibu Betty Hartati</td>
                                        </tr>
                                        <tr>
                                            <td colspan="3" style="text-align: right;">Jumlah Potongan</td>
                                            <th style="text-align: right; border-bottom: 1px solid;"><span id="jmlpotong"></span></th>
                                            <th colspan="2">&nbsp;</th>
                                        </tr>
                                        <tr>
                                            <th colspan="3" style="text-align: right;">Gaji dibayarkan</th>
                                            <th style="text-align: right;"><span id="gajiakhir"></span></th>
                                            <th colspan="2">&nbsp;</th>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="form-group col-md-4 col-12">
                                    <a class="btn btn-secondary" href="<?= site_url('slipgaji') ?>">Back</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

<script>
    $(document).ready(function() {
        var jsonDataUrl = "<?= $url ?>";
        // Function to populate the form with JSON data
        function populateForm(data) {
            $("#id_slipgaji").val(data.idslip);
            $("#id_pegawai").text(data.nama_pegawai);
            $("#no_rek").text(data.no_rek);
            var gajiBulan = data.gaji_bulan;
            var dateComponents = gajiBulan.split('-');
            var formattedValue = dateComponents[0] + '-' + dateComponents[1];
            var dateParts = formattedValue.split('-');
            var year = dateParts[0];
            var month = dateParts[1];
            var date = new Date(year, month - 1);
            var monthNames = [
                "January", "February", "March", "April", "May", "June",
                "July", "August", "September", "October", "November", "December"
            ];
            var formattedDate = monthNames[date.getMonth()] + " " + date.getFullYear();

            function formatCurrencyWithoutSymbol(number) {
                // Check if the number is a decimal
                const isDecimal = (number % 1 !== 0);

                // Define formatting options
                const options = {
                    style: 'currency',
                    currency: 'IDR',
                    minimumFractionDigits: isDecimal ? 2 : 0, // Show 2 decimal places if it's a decimal, otherwise none
                    maximumFractionDigits: isDecimal ? 2 : 0
                };

                // Format the number
                const formattedNumber = new Intl.NumberFormat('id-ID', options).format(number);

                // Remove 'Rp' from the formatted string
                return formattedNumber.replace('Rp', '');
            }

            $("#gaji_bulan").text(formattedDate);
            $("#gaji").text(formatCurrencyWithoutSymbol(data.gaji));
            $("#ikahi_cab").text(formatCurrencyWithoutSymbol(data.ikahi_cab));
            $("#lain2").text(formatCurrencyWithoutSymbol(data.lain2));
            $("#arisan_gabungan").text(formatCurrencyWithoutSymbol(data.arisan_gabungan));
            $("#simpan_pinjam").text(formatCurrencyWithoutSymbol(data.simpan_pinjam));
            $("#iuran_dyk").text(formatCurrencyWithoutSymbol(data.iuran_dyk));
            $("#iuran_koperasi").text(formatCurrencyWithoutSymbol(data.iuran_koperasi));
            $("#ptwp").text(formatCurrencyWithoutSymbol(data.ptwp));
            $("#ipaspi").text(formatCurrencyWithoutSymbol(data.ipaspi));
            $("#pinjaman_koperasi").text(formatCurrencyWithoutSymbol(data.pinjaman_koperasi));
            $("#bapor").text(formatCurrencyWithoutSymbol(data.bapor));
            $("#kebersamaan_hakim").text(formatCurrencyWithoutSymbol(data.kebersamaan_hakim));
            $("#mushola").text(formatCurrencyWithoutSymbol(data.mushola));
            $("#bri_bsm_jabar").text(formatCurrencyWithoutSymbol(data.bri_bsm_jabar));
            $("#sewa_rumah").text(formatCurrencyWithoutSymbol(data.sewa_rumah));
            $("#iuran_hakim").text(formatCurrencyWithoutSymbol(data.iuran_hakim));

            var gj = data.gaji;
            var a1 = data.ikahi_cab;
            var a2 = data.lain2;
            var a3 = data.arisan_gabungan;
            var a4 = data.simpan_pinjam;
            var a5 = data.iuran_dyk;
            var a6 = data.iuran_koperasi;
            var a7 = data.ptwp;
            var a8 = data.ipaspi;
            var a9 = data.pinjaman_koperasi;
            var a10 = data.bapor;
            var a11 = data.kebersamaan_hakim;
            var a12 = data.mushola;
            var a13 = data.bri_bsm_jabar;
            var a14 = data.sewa_rumah;
            var a15 = data.iuran_hakim;
            var jmlpotong = parseFloat(a1) + parseFloat(a2) + parseFloat(a3) + parseFloat(a4) + parseFloat(a5) + parseFloat(a6) + parseFloat(a7) + parseFloat(a8) + parseFloat(a9) + parseFloat(a10) + parseFloat(a11) + parseFloat(a12) + parseFloat(a13) + parseFloat(a14) + parseFloat(a15);
            // Calculate gaji - jmlpotong
            var gajiakhir = parseFloat(gj) - parseFloat(jmlpotong);

            // Update the spans with the calculated values
            $("#jmlpotong").text(formatCurrencyWithoutSymbol(jmlpotong));
            $("#gajiakhir").text(formatCurrencyWithoutSymbol(gajiakhir));

        }

        // Fetch JSON data using AJAX
        $.ajax({
            url: jsonDataUrl,
            dataType: "json",
            success: function(data) {
                populateForm(data.data);
            },
            error: function() {
                console.log("Error fetching JSON data.");
            }
        });
    });
</script>

<script>
    function downloadPdf() {
        var printSectionContent = document.getElementById('print-section').innerHTML;
        var button = document.querySelector('.btn-download-pdf');
        button.style.display = 'none';

        var namaPegawai = $("#id_pegawai").text();
        var gajiBulan = $("#gaji_bulan").text();
        var filename = sanitizeFilename(`${namaPegawai}_${gajiBulan}.pdf`);

        html2pdf(printSectionContent, {
            margin: 10,
            filename: filename,
            image: {
                type: 'jpeg',
                quality: 1.00
            },
            html2canvas: {
                scale: 2
            },
            jsPDF: {
                unit: 'mm',
                format: 'a4',
                orientation: 'portrait'
            }
        }).then(function(pdf) {
            // pdf.save();
            setTimeout(function() {
                window.location.reload();
            }, 1000);
        });
    }

    function sanitizeFilename(filename) {
        return filename.replace(/[/\\?%*:|"<>]/g, '_');
    }
</script>