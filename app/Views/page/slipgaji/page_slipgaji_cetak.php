<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" type="image/png" sizes="32x32" href="<?= site_url() ?>img/favicon-32x32.png">
    <title><?= $title .' '?></title>
    <style>
        @media print {
            .page-break {
                page-break-before: always;
            }

            body * {
                visibility: hidden;
            }

            #print-section,
            #print-section * {
                visibility: visible;
            }

            #print-section {
                position: absolute;
                left: 0;
                top: 0;
            }

            @page {
                /* size: 105mm 148.5mm; */
                size: auto;
                margin: 1cm;
            }
        }
    </style>
</head>

<body>
    <div id="print-section">
        <h3 style="text-align: center;">SLIP GAJI</h3>
        <h5>PENGADILAN TINGGI DKI JAKARTA</h5>
        <table>
            <tr>
                <td>Pembayaran</td>
                <td> : <span id="gaji_bulan"></span></td>
            </tr>
            <tr>
                <td>Pegawai</td>
                <td> : <span id="id_pegawai"></span></td>
            </tr>
            <tr>
                <td>Rekening</td>
                <td> : <span id="no_rek"></span></td>
            </tr>
        </table>
        <hr>
        <div class="table-responsive">
            <table style="width: 100%;">
                <tr>
                    <th colspan="3" style="text-align: left;">Gaji bersih</th>
                    <th style="text-align: right;"><span id="gaji"></span></th>
                </tr>
                <tr>
                    <th colspan="4" style="text-align: left;">Potongan-potongan</th>
                </tr>
                <tr>
                    <td style="text-align:right">1.</td>
                    <td>IKAHI CAB & DAERAH</td>
                    <td style="text-align: right;"><span id="ikahi_cab"></span></td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td style="text-align:right">2.</td>
                    <td>LAIN-LAIN</td>
                    <td style="text-align: right;"><span id="lain2"></span></td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td style="text-align:right">3.</td>
                    <td>ARISAN GABUNGAN DYK</td>
                    <td style="text-align: right;"><span id="arisan_gabungan"></span></td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td style="text-align:right">4.</td>
                    <td>SIMPAN PINJAM</td>
                    <td style="text-align: right;"><span id="simpan_pinjam"></span></td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td style="text-align:right">5.</td>
                    <td>IURAN DYK</td>
                    <td style="text-align: right;"><span id="iuran_dyk"></span></td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td style="text-align:right">6.</td>
                    <td>IURAN KOPERASI</td>
                    <td style="text-align: right;"><span id="iuran_koperasi"></span></td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td style="text-align:right">7.</td>
                    <td>PTWP</td>
                    <td style="text-align: right;"><span id="ptwp"></span></td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td style="text-align:right">8.</td>
                    <td>IPASPI</td>
                    <td style="text-align: right;"><span id="ipaspi"></span></td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td style="text-align:right">9.</td>
                    <td>PINJAMAN KOPERASI</td>
                    <td style="text-align: right;"><span id="pinjaman_koperasi"></span></td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td style="text-align:right">10.</td>
                    <td>BAPOR</td>
                    <td style="text-align: right;"><span id="bapor"></span></td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td style="text-align:right">11.</td>
                    <td>KEBERSAMAAN HAKIM</td>
                    <td style="text-align: right;"><span id="kebersamaan_hakim"></span></td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td style="text-align:right">12.</td>
                    <td>MUSHOLA</td>
                    <td style="text-align: right;"><span id="mushola"></span></td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td style="text-align:right">13.</td>
                    <td>BRI/BSM/JABAR</td>
                    <td style="text-align: right;"><span id="bri_bsm_jabar"></span></td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td style="text-align:right">14.</td>
                    <td>SEWA RUMAH</td>
                    <td style="text-align: right;"><span id="sewa_rumah"></span></td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td style="text-align:right">15.</td>
                    <td>IURAN DYK HAKIM/BPDSH</td>
                    <td style="text-align: right;"><span id="iuran_hakim"></span></td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="3" style="text-align: right;">Jumlah Potongan</td>
                    <th style="text-align: right; border-bottom: 1px solid;"><span id="jmlpotong"></span></th>
                </tr>
                <tr>
                    <th colspan="3" style="text-align: right;">Gaji dibayarkan</th>
                    <th style="text-align: right;"><span id="gajiakhir"></span></th>
                </tr>
            </table>
        </div>
    </div>
    <script src="<?= site_url() ?>assets/modules/jquery.min.js"></script>
    <script>
        $(document).ready(function() {
            var jsonDataUrl = "<?= $url ?>";
            // alert(jsonDataUrl);
            // Function to populate the form with JSON data
            function populateForm(data) {
                $("#id_slipgaji").val(data.id);
                $("#id_pegawai").text(data.nama_pegawai);
                document.title = "<?= $title ?> " + data.nama_pegawai;
                $("#no_rek").text(data.no_rek);
                var gajiBulan = data.gaji_bulan;
                var dateComponents = gajiBulan.split('-');
                var formattedValue = dateComponents[0] + '-' + dateComponents[1];
                var dateParts = formattedValue.split('-');
                var year = dateParts[0];
                var month = dateParts[1];
                var date = new Date(year, month - 1);
                var monthNames = [
                    "January", "February", "March", "April", "May", "June",
                    "July", "August", "September", "October", "November", "December"
                ];
                var formattedDate = monthNames[date.getMonth()] + " " + date.getFullYear();

                function formatCurrencyWithoutSymbol(number) {
                    return new Intl.NumberFormat('id-ID', {
                            style: 'currency',
                            currency: 'IDR'
                        })
                        .format(number)
                        .replace('Rp', ''); // Remove 'Rp' from the formatted string
                }

                $("#gaji_bulan").text(formattedDate);
                $("#gaji").text(formatCurrencyWithoutSymbol(data.gaji));
                $("#ikahi_cab").text(formatCurrencyWithoutSymbol(data.ikahi_cab));
                $("#lain2").text(formatCurrencyWithoutSymbol(data.lain2));
                $("#arisan_gabungan").text(formatCurrencyWithoutSymbol(data.arisan_gabungan));
                $("#simpan_pinjam").text(formatCurrencyWithoutSymbol(data.simpan_pinjam));
                $("#iuran_dyk").text(formatCurrencyWithoutSymbol(data.iuran_dyk));
                $("#iuran_koperasi").text(formatCurrencyWithoutSymbol(data.iuran_koperasi));
                $("#ptwp").text(formatCurrencyWithoutSymbol(data.ptwp));
                $("#ipaspi").text(formatCurrencyWithoutSymbol(data.ipaspi));
                $("#pinjaman_koperasi").text(formatCurrencyWithoutSymbol(data.pinjaman_koperasi));
                $("#bapor").text(formatCurrencyWithoutSymbol(data.bapor));
                $("#kebersamaan_hakim").text(formatCurrencyWithoutSymbol(data.kebersamaan_hakim));
                $("#mushola").text(formatCurrencyWithoutSymbol(data.mushola));
                $("#bri_bsm_jabar").text(formatCurrencyWithoutSymbol(data.bri_bsm_jabar));
                $("#sewa_rumah").text(formatCurrencyWithoutSymbol(data.sewa_rumah));
                $("#iuran_hakim").text(formatCurrencyWithoutSymbol(data.iuran_hakim));

                var gj = data.gaji;
                var a1 = data.ikahi_cab;
                var a2 = data.lain2;
                var a3 = data.arisan_gabungan;
                var a4 = data.simpan_pinjam;
                var a5 = data.iuran_dyk;
                var a6 = data.iuran_koperasi;
                var a7 = data.ptwp;
                var a8 = data.ipaspi;
                var a9 = data.pinjaman_koperasi;
                var a10 = data.bapor;
                var a11 = data.kebersamaan_hakim;
                var a12 = data.mushola;
                var a13 = data.bri_bsm_jabar;
                var a14 = data.sewa_rumah;
                var a15 = data.iuran_hakim;
                var jmlpotong = parseFloat(a1) + parseFloat(a2) + parseFloat(a3) + parseFloat(a4) + parseFloat(a5) + parseFloat(a6) + parseFloat(a7) + parseFloat(a8) + parseFloat(a9) + parseFloat(a10) + parseFloat(a11) + parseFloat(a12) + parseFloat(a13) + parseFloat(a14) + parseFloat(a15);
                // Calculate gaji - jmlpotong
                var gajiakhir = parseFloat(gj) - parseFloat(jmlpotong);

                // Update the spans with the calculated values
                $("#jmlpotong").text(formatCurrencyWithoutSymbol(jmlpotong));
                $("#gajiakhir").text(formatCurrencyWithoutSymbol(gajiakhir));

            }

            // Fetch JSON data using AJAX
            console.log('Requesting data from:', jsonDataUrl);

            // Fetch JSON data using AJAX
            $.ajax({
                url: jsonDataUrl,
                dataType: "json",
                success: function(data) {
                    console.log('Received data:', data);
                    populateForm(data.data);
                    window.print();
                },
                error: function(error) {
                    console.error("Error fetching JSON data:", error);
                }
            });

        });
    </script>
</body>

</html>