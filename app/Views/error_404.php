<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>404 Not Found</title>
    <style>
        body {
            font-family: 'Arial', sans-serif;
            background-color: #f4f4f4;
            margin: 0;
            padding: 0;
            display: flex;
            align-items: center;
            justify-content: center;
            height: 100vh;
        }

        .error-container {
            text-align: center;
        }

        .error-code {
            font-size: 8em;
            color: #333;
            margin: 0;
        }

        .error-message {
            font-size: 1.5em;
            color: #555;
        }

        .back-home-link {
            text-decoration: none;
            color: #007BFF;
            font-weight: bold;
            margin-top: 20px;
            display: inline-block;
            padding: 10px 20px;
            border: 2px solid #007BFF;
            border-radius: 5px;
            transition: background-color 0.3s, color 0.3s;
        }

        .back-home-link:hover {
            background-color: #007BFF;
            color: #fff;
        }
    </style>
</head>

<body>
    <div class="error-container">
        <h1 class="error-code">404</h1>
        <p class="error-message">Page not found</p>
        <a href="#" class="back-home-link">Back to Home</a>
    </div>
</body>

</html>