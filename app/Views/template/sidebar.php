<div class="main-sidebar sidebar-style-2">
    <aside id="sidebar-wrapper">
        <div class="sidebar-brand" style="background-image: url('<?= site_url() ?>img/background2.jpg');">
            <a href="<?php echo session('status') === '1' ? site_url('dashboard') : site_url('dashboard2') ?>"><span id="namasidebar">Stisla</span></a>
        </div>
        <div class="sidebar-brand sidebar-brand-sm">
            <a href="<?php echo session('status') === '1' ? site_url('dashboard') : site_url('dashboard2') ?>"><span id="aliassidebar">St</span></a>
        </div>
        <ul class="sidebar-menu">
            <!-- <li class="menu-header">Dashboard</li> -->
            <?php if (session('status') === '1') : ?>
                <li class="active"><a class="nav-link" href="<?php echo session('status') === '1' ? site_url('dashboard') : site_url('dashboard2') ?>"><i class="fas fa-tachometer-alt"></i> <span>Dashboard</span></a></li>


                <li class="menu-header">Master</li>
                <li class="dropdown">
                    <a href="#" class="nav-link has-dropdown" data-toggle="dropdown"><i class="fas fa-th-large"></i> <span>Master Data</span></a>
                    <ul class="dropdown-menu">
                        <li><a class="nav-link" href="<?= site_url('pegawai') ?>">Pegawai Aktif</a></li>
                        <li><a class="nav-link" href="<?= site_url('pegawai0') ?>">Pegawai Non Aktif</a></li>
                        <li><a class="nav-link" href="<?= site_url('potongan') ?>">Potongan</a></li>
                    </ul>
                </li>
            <?php endif; ?>

            <!-- <li class="menu-header">Payment</li>
            <li ><a class="nav-link" href="<?= site_url('payment') ?>"><i class="fas fa-money-bill-wave"></i> <span>Payment</span></a></li> -->
            <li class="menu-header">Slip Gaji</li>
            <li><a class="nav-link" href="<?= site_url('slipgaji') ?>"><i class="fas fa-money-bill-wave"></i> <span>Slip Gaji</span></a></li>

            <?php if (session('status') === '1') : ?>
                <li class="menu-header">Settings</li>
                <li class="dropdown">
                    <a href="#" class="nav-link has-dropdown" data-toggle="dropdown"><i class="fas fa-cogs"></i> <span>Features</span></a>
                    <ul class="dropdown-menu">
                        <!-- <li><a class="nav-link" href="<?= site_url('user') ?>">Akun Admin</a></li> -->
                        <li><a class="nav-link" href="<?= site_url('userp') ?>">Akun Pegawai</a></li>
                        <li><a class="nav-link" href="<?= site_url('about') ?>">Settings</a></li>
                    </ul>
                </li>
            <?php endif; ?>
        </ul>
    </aside>
</div>