<?php

namespace App\Controllers;

use App\Models\PegawaiModel;
use CodeIgniter\Controller;

class SlipGaji extends Controller
{
    public function index()
    {
        if (!session('logged_in')) {
            return redirect()->to('/');
        }

        $userEmail = session('user_email'); // Fetch the user's email
        $pegawaiModel = new PegawaiModel();

        // Initialize $pegawaiId to an empty string
        $pegawaiId = '';

        if (session('status') === '0') {
            $pegawaiData = $pegawaiModel->where('nip', $userEmail)->first();

            if ($pegawaiData) {
                $pegawaiId = $pegawaiData['id'];
            }
        }

        $isi = [
            'title' => 'Slip Gaji',
        ];

        if (session('status') === '0' && !empty($pegawaiId)) {
            $isi['url'] = site_url('api/slipgaji2/') . $pegawaiId;
        } else {
            $isi['url'] = site_url('api/slipgaji');
        }

        $data = [
            'title' => 'Slip Gaji',
            'content' => view('page/slipgaji/page_slipgaji', $isi),
        ];
        echo view('template/table_template', $data);
    }


    public function tambah()
    {
        if (!session('logged_in')) {
            return redirect()->to('/');
        }
        $isi = [
            'title' => 'Slip Gaji',
            'url'   => site_url('api/slipgaji'),
            'url_pegawai'   => site_url('api/pegawai'),
        ];
        $data = [
            'title' => 'Tambah Slip Gaji',
            'content' => view('page/slipgaji/page_slipgaji_add', $isi),
        ];
        echo view('template/main_template', $data);
    }

    public function edit($id)
    {
        if (!session('logged_in')) {
            return redirect()->to('/');
        }
        $isi = ['title' => 'Slip Gaji', 'url'   => site_url('api/slipgaji/') . base64_decode($id), 'url_pegawai'   => site_url('api/pegawai')];
        $data = [
            'title' => 'Edit Slip Gaji',
            'content' => view('page/slipgaji/page_slipgaji_edit', $isi),
        ];
        echo view('template/main_template', $data);
    }

    public function lihat($id)
    {
        if (!session('logged_in')) {
            return redirect()->to('/');
        }
        $isi = ['title' => 'Detail Slip Gaji', 'url'   => site_url('api/slipgaji/') . base64_decode($id),];
        $data = [
            'title' => 'Detail Slip Gaji',
            'content' => view('page/slipgaji/page_slipgaji_lihat', $isi),
        ];
        echo view('template/main_template', $data);
    }

    public function cetak($id)
    {
        if (!session('logged_in')) {
            return redirect()->to('/');
        }
        $isi = ['title' => 'Detail Slip Gaji', 'url'   => site_url('api/slipgaji/') . base64_decode($id),];
        echo view('page/slipgaji/page_slipgaji_cetak', $isi);
    }
}
