<?php

namespace App\Controllers\Api;

use CodeIgniter\RESTful\ResourceController;

class UserController extends ResourceController
{
    protected $modelName = 'App\Models\UserModel';
    protected $format    = 'json';

    // GET all users
    public function index()
    {
        $users = $this->model->findAll();
        return $this->respond(['message' => 'Users retrieved successfully', 'data' => $users]);
    }

    public function indexw()
    {
        $users = $this->model->where('status', '1')->findAll();
        return $this->respond(['message' => 'Users retrieved successfully', 'data' => $users]);
    }

    public function indexw0()
    {
        $users = $this->model->where('status', '0')->findAll();
        return $this->respond(['message' => 'Users retrieved successfully', 'data' => $users]);
    }

    // GET single user
    public function show($id = null)
    {
        $user = $this->model->find($id);
        if (!$user) {
            return $this->failNotFound('User not found');
        }
        return $this->respond(['message' => 'User retrieved successfully', 'data' => $user]);
    }

    // POST create user with image upload
    public function create()
    {
        $data = $this->request->getPost();

        // Validate if the username already exists
        if ($this->model->where('username', $data['username'])->first()) {
            return $this->fail('Username already exists', 400);
        }

        $image = $this->request->getFile('profile_image');

        if ($image->isValid() && !$image->hasMoved()) {
            $newName = $image->getRandomName();
            $image->move(FCPATH . 'uploads', $newName);
            $data['profile_image'] = 'uploads/' . $newName;
        } else {
            unset($data['profile_image']);
        }

        $data['pass'] = password_hash($data['pass'], PASSWORD_BCRYPT);

        // Insert the user data into the database
        $cek = $this->model->insert($data);
        if ($cek) {
            return $this->respondCreated(['message' => 'User created successfully', 'data' => $data]);
        } else {
            return $this->respondCreated(['message' => 'Error add data user.']);
            // return $this->failNotFound('Error add data user.');
        }
    }


    // PUT update user with image upload
    public function update($id = null)
    {
        $data = $this->request->getVar(); // Get regular data
        $image = $this->request->getFile('profile_image');

        if ($image->isValid() && !$image->hasMoved()) {
            $newName = $image->getRandomName();
            $image->move(FCPATH . 'uploads', $newName);
            $data['profile_image'] = 'uploads/' . $newName;

            // Delete the old image if it exists
            $oldImage = $this->model->find($id)['profile_image'];
            if ($oldImage && file_exists(FCPATH . '' . $oldImage)) {
                unlink(FCPATH . '' . $oldImage);
            }
        } else {
            unset($data['profile_image']);
        }

        if (empty($data['pass'])) {
            unset($data['pass']);
        } else {
            $data['pass'] = password_hash($data['pass'], PASSWORD_BCRYPT);
        }

        if (!$this->model->find($id)) {
            return $this->fail('User not found', 404);
        }

        $this->model->update($id, $data);

        // Fetch the updated user data
        $updatedUser = $this->model->find($id);

        return $this->respond([
            'message' => 'User updated successfully',
            'data' => $updatedUser
        ]);
    }

    // DELETE delete user
    public function delete($id = null)
    {
        $user = $this->model->find($id);

        if (!$user) {
            return $this->failNotFound('User not found');
        }

        // Delete the associated image if it exists
        $imagePath = FCPATH . '' . $user['profile_image'];
        if ($user['profile_image'] && file_exists($imagePath)) {
            unlink($imagePath);
        }

        $this->model->delete($id);

        return $this->respondDeleted(['message' => 'User deleted']);
    }

    // update password
    public function uppass($id = null)
    {
        // Fetch the existing user data
        $user = $this->model->find($id);
        $pass = $this->request->getVar('pass');

        if ($user) {
            // Get the updated data from the request
            $data = [
                'pass' => password_hash($pass, PASSWORD_BCRYPT),
            ];

            // Update the user record in the database
            $this->model->update($id, $data);
            return $this->respond([
                'messages' => ['success' => 'Password updated successfully'],
                'pass' => $pass,
                'phas' => $data['pass'],
            ]);
        } else {
            return $this->failNotFound('Data not found.');
        }
    }
}
