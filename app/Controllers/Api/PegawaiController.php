<?php

namespace App\Controllers\Api;

use App\Models\UserModel;
use CodeIgniter\RESTful\ResourceController;

class PegawaiController extends ResourceController
{
    protected $modelName = 'App\Models\PegawaiModel';
    protected $format    = 'json';

    // GET all pegawai
    public function index()
    {
        $pegawai = $this->model->where('status', '1')->orderBy('nama_pegawai', 'ASC')->findAll();
        return $this->respond(['message' => 'pegawai retrieved successfully', 'data' => $pegawai]);
    }

    public function index0()
    {
        $pegawai = $this->model->where('status', '0')->orderBy('nama_pegawai', 'ASC')->findAll();
        return $this->respond(['message' => 'pegawai retrieved successfully', 'data' => $pegawai]);
    }


    // GET single pegawai
    public function show($id = null)
    {
        $pegawai = $this->model->find($id);
        if (!$pegawai) {
            return $this->failNotFound('pegawai not found');
        }
        return $this->respond(['message' => 'pegawai retrieved successfully', 'data' => $pegawai]);
    }

    // POST create pegawai with image upload
    public function create()
    {
        $data = $this->request->getPost();
        // Insert the pegawai data into the database
        $pegawai = $this->model->insert($data);

        return $this->respondCreated(['message' => 'pegawai created successfully', 'data' => $data]);
    }


    // PUT update pegawai with image upload
    public function update($id = null)
    {
        $data = $this->request->getVar(); // Get regular data

        if (!$this->model->find($id)) {
            return $this->fail('pegawai not found', 404);
        }

        $this->model->update($id, $data);

        return $this->respond([
            'message' => 'pegawai updated successfully',
            'data' => $data
        ]);
    }

    // DELETE delete pegawai
    public function delete($id = null)
    {
        $pegawai = $this->model->find($id);

        if (!$pegawai) {
            return $this->failNotFound('pegawai not found');
        }

        if ($pegawai) {
            $pegawaiNIP = $pegawai['nip'];
        }

        $userModel = new UserModel();
        $userId = '';
        $userData = $userModel->where('email', $pegawaiNIP)->first();

        if ($userData) {
            $userId = $userData['id'];
        }

        $this->model->delete($id);
        $userModel->delete($userId);

        return $this->respondDeleted(['message' => 'pegawai deleted']);
    }

    // update status pegawai dari 1 ke 0
    public function updatestatus($id = null)
    {
        $data = $this->request->getVar();
        $pegawai = $this->model->find($id);
        if (!$pegawai) {
            return $this->fail('Pegawai not found', 404);
        } else {
            $this->model->update($id, $data);
            return $this->respond([
                'message' => 'Pegawai status updated successfully',
                'data' => $data,
            ]);
        }
    }
}
