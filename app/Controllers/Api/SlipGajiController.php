<?php

namespace App\Controllers\Api;

use CodeIgniter\RESTful\ResourceController;

class SlipGajiController extends ResourceController
{
    protected $modelName = 'App\Models\SlipGajiModel';
    protected $format    = 'json';

    // GET all slipgaji
    public function index()
    {
        // Specify the columns you want to select from both tables
        $this->model->select('tbl_slipgaji.*, tbl_pegawai.nip, tbl_pegawai.*, tbl_slipgaji.id as idslip');
        // Perform the inner join on 'tbl_pegawai' and 'tbl_slipgaji' using 'id' as the common key
        $this->model->join('tbl_pegawai', 'tbl_pegawai.id = tbl_slipgaji.id_pegawai', 'inner');
        // Retrieve the data
        $slipgaji = $this->model->findAll();
        return $this->respond(['message' => 'slipgaji retrieved successfully', 'data' => $slipgaji]);
    }

    public function index2($id = null)
    {
        $this->model->select('tbl_slipgaji.*, tbl_pegawai.nip, tbl_pegawai.*, tbl_slipgaji.id as idslip');
        $this->model->join('tbl_pegawai', 'tbl_pegawai.id = tbl_slipgaji.id_pegawai', 'inner');
        $this->model->where('tbl_slipgaji.id_pegawai', $id);
        $slipgaji = $this->model->findAll();
        return $this->respond(['message' => 'slipgaji retrieved successfully', 'data' => $slipgaji]);
    }


    // GET single slipgaji with join
    public function show($id = null)
    {
        // Specify the columns you want to select from both tables
        $this->model->select('tbl_slipgaji.*, tbl_pegawai.*, tbl_slipgaji.id as idslip');
        // Perform the inner join on 'tbl_pegawai' and 'tbl_slipgaji' using 'id' as the common key
        $this->model->join('tbl_pegawai', 'tbl_pegawai.id = tbl_slipgaji.id_pegawai', 'inner');
        // Find the data for the specified ID
        $slipgaji = $this->model->find($id);
        if (!$slipgaji) {
            return $this->failNotFound('slip gaji not found');
        }
        return $this->respond(['message' => 'slip gaji retrieved successfully', 'data' => $slipgaji]);
    }


    // POST create slipgaji with image upload
    public function create()
    {
        $data = $this->request->getPost();

        // Check if the combination of gaji_bulan and id_pegawai already exists
        $existingRecord = $this->model->where('gaji_bulan', $data['gaji_bulan'])
            ->where('id_pegawai', $data['id_pegawai'])
            ->first();

        if ($existingRecord) {
            // If the record already exists, respond with an error message
            return $this->respond(['messages' => ['error' => 'Slip gaji for this month and employee already exists']]);
        }

        // If the record does not exist, insert the slipgaji data into the database
        $slipgaji = $this->model->insert($data);

        return $this->respondCreated(['messages' => ['success' => 'Slip gaji created successfully'], 'data' => $data]);
    }



    // PUT update slipgaji with image upload
    public function update($id = null)
    {
        $data = $this->request->getVar(); // Get regular data

        if (!$this->model->find($id)) {
            return $this->fail('slip gaji not found', 404);
        }

        $this->model->update($id, $data);

        return $this->respond([
            'message' => 'slip gaji updated successfully',
            'data' => $data
        ]);
    }

    // DELETE delete slipgaji
    public function delete($id = null)
    {
        $slipgaji = $this->model->find($id);

        if (!$slipgaji) {
            return $this->failNotFound('slip gaji not found');
        }

        $this->model->delete($id);

        return $this->respondDeleted(['message' => 'slip gaji deleted']);
    }
}
