<?php
namespace App\Controllers\Api;

use CodeIgniter\RESTful\ResourceController;

class AboutController extends ResourceController
{
    protected $modelName = 'App\Models\AboutModel';
    protected $format    = 'json';

    // GET all about
    public function index()
    {
        $about = $this->model->findAll();
        return $this->respond(['message' => 'About retrieved successfully', 'data' => $about]);
    }

    // GET single about
    public function show($id = null)
    {
        $about = $this->model->find($id);
        if (!$about) {
            return $this->failNotFound('About not found');
        }
        return $this->respond(['message' => 'About retrieved successfully', 'data' => $about]);
    }

    // POST create About with image upload
    public function create()
    {
        $data = $this->request->getPost();
        $image = $this->request->getFile('logo');

        if ($image->isValid() && !$image->hasMoved()) {
            $newName = $image->getRandomName();
            $image->move(FCPATH . 'uploads', $newName);
            $data['logo'] = 'uploads/' . $newName;
        } else {
            unset($data['logo']);
        }
        // Insert the About data into the database
        $this->model->insert($data);

        return $this->respondCreated(['message' => 'About created successfully', 'data' => $data]);
    }


    // PUT update About with image upload
    public function update($id = null)
    {
        $data = $this->request->getVar(); // Get regular data
        $image = $this->request->getFile('logo');

        if ($image->isValid() && !$image->hasMoved()) {
            $newName = $image->getRandomName();
            $image->move(FCPATH . 'uploads', $newName);
            $data['logo'] = 'uploads/' . $newName;

            // Delete the old image if it exists
            $oldImage = $this->model->find($id)['logo'];
            if ($oldImage && file_exists(FCPATH . '' . $oldImage)) {
                unlink(FCPATH . '' . $oldImage);
            }
        } else {
            unset($data['logo']);
        }
        if (!$this->model->find($id)) {
            return $this->fail('About not found', 404);
        }

        $this->model->update($id, $data);

        // Fetch the updated About data
        $updatedAbout = $this->model->find($id);

        return $this->respond([
            'message' => 'About updated successfully',
            'data' => $updatedAbout
        ]);
    }
}
