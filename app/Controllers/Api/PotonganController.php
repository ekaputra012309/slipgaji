<?php

namespace App\Controllers\Api;

use CodeIgniter\RESTful\ResourceController;

class PotonganController extends ResourceController
{
    protected $modelName = 'App\Models\PotonganModel';
    protected $format    = 'json';

    // GET all potongan
    public function index()
    {
        // Specify the columns you want to select from both tables
        $this->model->select('tbl_potongan.*, tbl_pegawai.nip, tbl_pegawai.*, tbl_potongan.id_potongan as idpotongan');
        // Perform the inner join on 'tbl_pegawai' and 'tbl_potongan' using 'id' as the common key
        $this->model->join('tbl_pegawai', 'tbl_pegawai.id = tbl_potongan.id_pegawai', 'inner');
        // Retrieve the data
        $potongan = $this->model->findAll();
        return $this->respond(['message' => 'potongan retrieved successfully', 'data' => $potongan]);
    }

    public function index2($id = null)
    {
        $this->model->select('tbl_potongan.*, tbl_pegawai.nip, tbl_pegawai.*, tbl_potongan.id_potongan as idpotongan');
        $this->model->join('tbl_pegawai', 'tbl_pegawai.id = tbl_potongan.id_pegawai', 'inner');
        $this->model->where('tbl_potongan.id_pegawai', $id);
        $potongan = $this->model->findAll();
        return $this->respond(['message' => 'potongan retrieved successfully', 'data' => $potongan]);
    }


    // GET single potongan with join
    public function show($id = null)
    {
        // Specify the columns you want to select from both tables
        $this->model->select('tbl_potongan.*, tbl_pegawai.*, tbl_potongan.id_potongan as idpotongan');
        // Perform the inner join on 'tbl_pegawai' and 'tbl_potongan' using 'id' as the common key
        $this->model->join('tbl_pegawai', 'tbl_pegawai.id = tbl_potongan.id_pegawai', 'inner');
        // Find the data for the specified ID
        $potongan = $this->model->find($id);
        if (!$potongan) {
            return $this->failNotFound('potongan not found');
        }
        return $this->respond(['message' => 'potongan retrieved successfully', 'data' => $potongan]);
    }


    // POST create potongan with image upload
    public function create()
    {
        $data = $this->request->getPost();
        // Insert the potongan data into the database
        $potongan = $this->model->insert($data);

        return $this->respondCreated(['message' => 'potongan created successfully', 'data' => $data]);
    }


    // PUT update potongan with image upload
    public function update($id = null)
    {
        $data = $this->request->getVar(); // Get regular data

        if (!$this->model->find($id)) {
            return $this->fail('potongan not found', 404);
        }

        $this->model->update($id, $data);

        return $this->respond([
            'message' => 'potongan updated successfully',
            'data' => $data
        ]);
    }

    // DELETE delete potongan
    public function delete($id = null)
    {
        $potongan = $this->model->find($id);

        if (!$potongan) {
            return $this->failNotFound('potongan not found');
        }

        $this->model->delete($id);

        return $this->respondDeleted(['message' => 'potongan deleted']);
    }
}
