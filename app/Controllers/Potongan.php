<?php

namespace App\Controllers;

use App\Models\PegawaiModel;
use CodeIgniter\Controller;

class Potongan extends Controller
{
    public function index()
    {
        if (!session('logged_in')) {
            return redirect()->to('/');
        }
        $isi = [
            'title' => 'Potongan',
            'url' => site_url('api/potongan'),
        ];
        $data = [
            'title' => 'Potongan',
            'content' => view('page/potongan/page_potongan', $isi),
        ];
        echo view('template/table_template', $data);
    }


    public function tambah()
    {
        if (!session('logged_in')) {
            return redirect()->to('/');
        }
        $isi = ['title' => 'Potongan', 'url'   => site_url('api/potongan'), 'url_pegawai'   => site_url('api/pegawai'),];
        $data = [
            'title' => 'Tambah Potongan',
            'content' => view('page/potongan/page_potongan_add', $isi),
        ];
        echo view('template/main_template', $data);
    }

    public function edit($id)
    {
        if (!session('logged_in')) {
            return redirect()->to('/');
        }
        $isi = ['title' => 'Potongan', 'url'   => site_url('api/potongan/') . base64_decode($id), 'url_pegawai'   => site_url('api/pegawai')];
        $data = [
            'title' => 'Edit Potongan',
            'content' => view('page/potongan/page_potongan_edit', $isi),
        ];
        echo view('template/main_template', $data);
    }

    public function lihat($id)
    {
        if (!session('logged_in')) {
            return redirect()->to('/');
        }
        $isi = ['title' => 'Detail Potongan', 'url'   => site_url('api/potongan/') . base64_decode($id),];
        $data = [
            'title' => 'Detail Potongan',
            'content' => view('page/potongan/page_potongan_lihat', $isi),
        ];
        echo view('template/main_template', $data);
    }

    public function cetak($id)
    {
        if (!session('logged_in')) {
            return redirect()->to('/');
        }
        $isi = ['title' => 'Detail Potongan', 'url'   => site_url('api/potongan/') . base64_decode($id),];
        echo view('page/potongan/page_potongan_cetak', $isi);
    }
}
