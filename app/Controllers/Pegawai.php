<?php

namespace App\Controllers;

use App\Models\PegawaiModel;
use App\Models\UserModel;
use CodeIgniter\Controller;

class Pegawai extends Controller
{
    public function index()
    {
        if (!session('logged_in')) {
            return redirect()->to('/');
        }
        $isi = [
            'title' => 'Pegawai',
            'url'   => site_url('api/pegawai'),
            'urlS'   => site_url('api/pegawais')
        ];
        $data = [
            'title' => 'Pegawai',
            'content' => view('page/pegawai/page_pegawai', $isi),
        ];
        echo view('template/table_template', $data);
    }

    public function index0()
    {
        if (!session('logged_in')) {
            return redirect()->to('/');
        }
        $isi = [
            'title' => 'Pegawai',
            'url'   => site_url('api/pegawai0'),
            'urlS'   => site_url('api/pegawais')
        ];
        $data = [
            'title' => 'Pegawai',
            'content' => view('page/pegawai/page_pegawai_nonaktif', $isi),
        ];
        echo view('template/table_template', $data);
    }

    public function tambah()
    {
        if (!session('logged_in')) {
            return redirect()->to('/');
        }
        $isi = ['title' => 'Pegawai', 'url'   => site_url('api/pegawai')];
        $data = [
            'title' => 'Pegawai Add',
            'content' => view('page/pegawai/page_pegawai_add', $isi),
        ];
        echo view('template/main_template', $data);
    }

    public function edit($id)
    {
        $newId = base64_decode($id);
        if (!session('logged_in')) {
            return redirect()->to('/');
        } // Fetch the user's email
        $pegawaiModel = new PegawaiModel();
        $pegawaiNIP = '';
        $pegawaiData = $pegawaiModel->where('id', $newId)->first();

        if ($pegawaiData) {
            $pegawaiNIP = $pegawaiData['nip'];
        }

        $userModel = new UserModel();
        $userId = '';
        $userData = $userModel->where('email', $pegawaiNIP)->first();

        if ($userData) {
            $userId = $userData['id'];
        }

        $isi = [
            'title' => 'Pegawai',
            'url'   => site_url('api/pegawai/') . $newId,
            'urluser'   => site_url('api/users/') . $userId,
        ];
        $data = [
            'title' => 'Pegawai Edit',
            'content' => view('page/pegawai/page_pegawai_edit', $isi),
        ];
        echo view('template/main_template', $data);
    }
}
