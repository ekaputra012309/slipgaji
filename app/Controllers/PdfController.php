<?php

namespace App\Controllers;

use App\Models\CicilanModel;
use App\Models\PaymentModel;
use Dompdf\Dompdf;
use Dompdf\Options;

class PdfController extends BaseController
{
    // protected $modelName = 'App\Models\CicilanModel';

    public function generatePdf($id)
    {
        // Load Dompdf options
        $options = new Options();
        $options->set('isHtml5ParserEnabled', true);
        $options->set('isPhpEnabled', true);

        // Initialize Dompdf
        $dompdf = new Dompdf($options);

        // Fetch data from the model
        $cicilanModel = new CicilanModel(); // Adjust as per your model name and namespace
        $cicilan = $cicilanModel->where('id_jamaah', $id)->findAll();
        $saldo = 0;

        // Iterate through the cicilan data and calculate saldo for each row
        foreach ($cicilan as &$row) {
            // Convert 'nominal' to a numeric value
            $nominal = floatval($row['nominal']);
            $saldo += $nominal;
            $row['saldo'] = $saldo;
        }
        $data['cicilan'] = $cicilan;

        $paymentModel = new PaymentModel(); // Replace 'PaymentModel' with your actual model name
        $payment = $paymentModel
            ->select('*')
            ->join('tbl_jamaah', 'tbl_jamaah.id = tbl_payment.id_jamaah', 'left')
            ->join('tbl_paket', 'tbl_paket.id = tbl_payment.id_paket', 'left')
            ->where('tbl_payment.id_jamaah', $id)
            ->findAll();

        $data['payment'] = $payment;

        // Load HTML content with data
        $html = view('pdf_view', $data);

        // Load HTML into Dompdf
        $dompdf->loadHtml($html);

        // Set paper size and orientation
        $dompdf->setPaper('A4', 'portrait');

        // Render the PDF
        $dompdf->render();

        // Output the generated PDF to the browser
        $dompdf->stream('sample.pdf', ['Attachment' => 0]);
    }
    
    public function generatePdf1($id1)
    {
        $id = base64_decode($id1);
        $cicilanModel = new CicilanModel(); // Adjust as per your model name and namespace
        $cicilan = $cicilanModel->where('id_jamaah', $id)->findAll();
        $saldo = 0;

        // Iterate through the cicilan data and calculate saldo for each row
        foreach ($cicilan as &$row) {
            // Convert 'nominal' to a numeric value
            $nominal = floatval($row['nominal']);
            $saldo += $nominal;
            $row['saldo'] = $saldo;
        }

        $paymentModel = new PaymentModel(); // Replace 'PaymentModel' with your actual model name
        $payment = $paymentModel
            ->select('*')
            ->join('tbl_jamaah', 'tbl_jamaah.id = tbl_payment.id_jamaah', 'left')
            ->join('tbl_paket', 'tbl_paket.id = tbl_payment.id_paket', 'left')
            ->where('tbl_payment.id_jamaah', $id)
            ->findAll();
        $data = [
            'title' => 'Detail Payment',
            'cicilan' => $cicilan,
            'payment' => $payment,
        ];
        echo view('pdf_view1', $data);
    }
}
