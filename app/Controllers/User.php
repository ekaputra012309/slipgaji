<?php

namespace App\Controllers;

use CodeIgniter\Controller;

class User extends Controller
{
    public function index()
    {
        if (!session('logged_in')) {
            return redirect()->to('/');
        }
        $isi = ['title' => 'Users', 'url'   => site_url('api/usersw')];
        $data = [
            'title' => 'Users',
            'content' => view('page/users/page_users', $isi),
        ];
        echo view('template/table_template', $data);
    }

    public function indexp()
    {
        if (!session('logged_in')) {
            return redirect()->to('/');
        }
        $isi = [
            'title' => 'Akun Pegawai',
            'url'   => site_url('api/usersw0'),
            'urlpass'   => site_url('upass')
        ];
        $data = [
            'title' => 'Akun Pegawai',
            'content' => view('page/users/page_users_pegawai', $isi),
        ];
        echo view('template/table_template', $data);
    }

    public function tambah()
    {
        if (!session('logged_in')) {
            return redirect()->to('/');
        }
        $isi = ['title' => 'Users', 'url'   => site_url('api/users')];
        $data = [
            'title' => 'Users Add',
            'content' => view('page/users/page_users_add', $isi),
        ];
        echo view('template/main_template', $data);
    }

    public function tambah2()
    {
        if (!session('logged_in')) {
            return redirect()->to('/');
        }
        $isi = ['title' => 'Users', 'url'   => site_url('api/users')];
        $data = [
            'title' => 'Users Add',
            'content' => view('page/users/page_users_add2', $isi),
        ];
        echo view('template/main_template', $data);
    }

    public function edit($id)
    {
        if (!session('logged_in')) {
            return redirect()->to('/');
        }
        $isi = ['title' => 'Users', 'url'   => site_url('api/users/') . base64_decode($id)];
        $data = [
            'title' => 'Users Edit',
            'content' => view('page/users/page_users_edit', $isi),
        ];
        echo view('template/main_template', $data);
    }

    public function edit2($id)
    {
        if (!session('logged_in')) {
            return redirect()->to('/');
        }
        $isi = ['title' => 'Users', 'url'   => site_url('api/users/') . base64_decode($id)];
        $data = [
            'title' => 'Users Edit',
            'content' => view('page/users/page_users_edit2', $isi),
        ];
        echo view('template/main_template', $data);
    }
}
