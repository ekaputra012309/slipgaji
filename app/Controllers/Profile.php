<?php

namespace App\Controllers;

use CodeIgniter\Controller;

class Profile extends Controller
{
    public function index()
    {
        if (!session('logged_in')) {
            return redirect()->to('/');
        }
        $isi = ['title' => 'Profile', 'url'   => site_url('api/users/') . $_SESSION['user_id']];
        $data = [
            'title' => 'Profile',
            'content' => view('page/page_profile', $isi),
        ];
        echo view('template/main_template', $data);
    }

    public function ubahpassword()
    {
        if (!session('logged_in')) {
            return redirect()->to('/');
        }
        $isi = ['title' => 'Ubah Password', 'url'   => site_url('api/users/') . $_SESSION['user_id']];
        $data = [
            'title' => 'Ubah Password',
            'content' => view('page/page_ubahpassword', $isi),
        ];
        echo view('template/main_template', $data);
    }
}
