<?php

namespace App\Controllers;

use CodeIgniter\Controller;

class About extends Controller
{
    public function index()
    {
        if (!session('logged_in')) {
            return redirect()->to('/');
        }
        $isi = ['title' => 'About', 'url'   => site_url('api/about/1')];
        $data = [
            'title' => 'About',
            'content' => view('page/page_about', $isi),
        ];
        echo view('template/main_template', $data);
    }
}
