<?php

namespace App\Controllers;

use CodeIgniter\Controller;
use App\Models\UserModel;
use App\Models\PegawaiModel;
use App\Models\SlipGajiModel;

class Dashboard extends Controller
{
    public function index()
    {
        if (!session('logged_in')) {
            return redirect()->to('/');
        }
        $userModel = new UserModel();
        $userCount = $userModel->where('status', '1')->countAllResults();
        $pegawaiModel = new PegawaiModel();
        $pegawaiCount = $pegawaiModel->countAll();

        $isi = [
            'userCount' => $userCount,
            'pegawaiCount' => $pegawaiCount,
        ];
        $data = [
            'title' => 'Dashboard',
            'content' => view('page/page_dashboard', $isi),
        ];
        echo view('template/main_template', $data);
    }

    public function index2()
    {
        if (!session('logged_in')) {
            return redirect()->to('/');
        }
        $nip = session('user_email');
        $userModel = new UserModel();
        $userCount = $userModel->where('status', '1')->countAllResults();
        $slipModel = new SlipGajiModel();
        $slipGaji = $slipModel->join('tbl_pegawai', 'tbl_pegawai.id = tbl_slipgaji.id_pegawai')
            ->where('tbl_pegawai.nip', $nip)
            ->countAllResults();

        $isi = [
            'userCount' => $userCount,
            'slipGaji' => $slipGaji,
        ];
        $data = [
            'title' => 'Dashboard',
            'content' => view('page/page_dashboard2', $isi),
        ];
        echo view('template/main_template', $data);
    }
}
