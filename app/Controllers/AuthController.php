<?php

namespace App\Controllers;

use App\Controllers\BaseController;
use CodeIgniter\API\ResponseTrait;
use App\Models\UserModel;
use App\Services\JwtService;

class AuthController extends BaseController
{
    protected $modelName;
    private $session;
    use ResponseTrait;

    public function __construct()
    {
        helper(['form', 'url', 'session']);
        $this->session = session();
    }

    public function index()
    {
        $data = [
            'title' => 'Login',
            'url'   => site_url('api/about/1'),
        ];

        echo view('page/page_login', $data);
    }

    public function login()
    {
        $userModel = new UserModel();
        $email = $this->request->getVar('email');
        $password = $this->request->getVar('pass');
        $data = $userModel->where('email', $email)->first();
        if ($data) {
            $pass = $data['pass'];
            $verify_pass = password_verify($password, $pass);
            if ($verify_pass) {
                $ses_data = [
                    'user_id'       => $data['id'],
                    'user_name'     => $data['username'],
                    'full_name'     => $data['fullname'],
                    'user_email'    => $data['email'],
                    'user_photo'    => $data['profile_image'],
                    'logged_in'     => TRUE,
                    'status'        => $data['status'],
                ];
                $this->session->set($ses_data);
                // return redirect()->route('dashboard');
                if ($data['status'] == '1') {
                    return redirect()->route('dashboard');
                } else {
                    return redirect()->route('slipgaji');
                }
            } else {
                session()->setFlashdata('msg', 'Wrong Password');
                return redirect()->to('/');
            }
        } else {
            session()->setFlashdata('msg', 'NIP not Found');
            return redirect()->to('/');
        }
    }

    public function logout()
    {
        $session = session();
        $session->destroy();
        return redirect()->to('/');
    }

    public function loginmobile()
    {
        // Get the user's credentials from the request
        $email = $this->request->getVar('email');
        $password = $this->request->getVar('password');

        $model = new UserModel();
        $user = $model->where('email', $email)->first();

        if (is_null($user)) {
            return $this->respond(['error' => 'Invalid username or password.'], 401);
        }

        $pwd_verify = password_verify($password, $user['pass']);

        if (!$pwd_verify) {
            return $this->respond(['error' => 'Invalid username or password.'], 401);
        }

        if (!$user || !password_verify($password, $user['pass'])) {
            log_message('debug', 'Password Verification Failed');
            return $this->response->setStatusCode(401)->setJSON(['message' => 'Unauthorized']);
        }

        // User is authenticated, you might generate a token or set a session here
        $token = JwtService::generateToken($user);
        $response = [
            'message' => 'Login Succesful',
            'token' => $token,
            'data' => $user,
        ];

        return $this->respond($response, 200);
    }
}
