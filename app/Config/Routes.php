<?php

use CodeIgniter\Router\RouteCollection;

/**
 * @var RouteCollection $routes
 */
// $routes->get('/', 'Home::index');
$routes->get('/', 'AuthController::index');
$routes->get('dashboard', 'Dashboard::index', ['as' => 'dashboard']);
$routes->get('dashboard2', 'Dashboard::index2', ['as' => 'dashboard2']);
$routes->post('auth/login', 'AuthController::login');
$routes->post('auth/mlogin', 'AuthController::loginmobile');
$routes->get('auth/logout', 'AuthController::logout');

$routes->get('profile', 'Profile::index');
$routes->get('ubahpassword', 'Profile::ubahpassword');
$routes->get('about', 'About::index');
$routes->get('user', 'User::index');
$routes->get('userp', 'User::indexp');
$routes->get('user/t', 'User::tambah');
$routes->get('user/t2', 'User::tambah2');
$routes->get('user/e/(:segment)', 'User::edit/$1');
$routes->get('user/e2/(:segment)', 'User::edit2/$1');
$routes->get('paket', 'Paket::index');
$routes->get('paket/t', 'Paket::tambah');
$routes->get('paket/e/(:segment)', 'Paket::edit/$1');
$routes->get('jamaah', 'Jamaah::index');
$routes->get('jamaah/t', 'Jamaah::tambah');
$routes->get('jamaah/e/(:segment)', 'Jamaah::edit/$1');
$routes->get('payment', 'Payment::index');
$routes->get('payment/t', 'Payment::tambah');
$routes->get('payment/l/(:segment)', 'Payment::lihat/$1');
$routes->get('payment/e/(:segment)', 'Payment::exportExcel/$1');
$routes->get('api/cicil/(:num)', 'Api\CicilanController::lihat/$1');
$routes->get('api/cidel/(:num)', 'Api\CicilanController::del/$1');
$routes->get('pdf/(:segment)', 'PdfController::generatePdf/$1');
$routes->get('gpdf/(:segment)', 'PdfController::generatePdf1/$1');

$routes->get('pegawai', 'Pegawai::index');
$routes->get('pegawai0', 'Pegawai::index0');
$routes->get('pegawai/t', 'Pegawai::tambah');
$routes->get('pegawai/e/(:segment)', 'Pegawai::edit/$1');
$routes->get('slipgaji', 'SlipGaji::index');
$routes->get('slipgaji/t', 'SlipGaji::tambah');
$routes->get('slipgaji/e/(:segment)', 'SlipGaji::edit/$1');
$routes->get('slipgaji/l/(:segment)', 'SlipGaji::lihat/$1');
$routes->get('slipgaji/c/(:segment)', 'SlipGaji::cetak/$1');
$routes->get('potongan', 'Potongan::index');
$routes->get('potongan/t', 'Potongan::tambah');
$routes->get('potongan/e/(:segment)', 'Potongan::edit/$1');

// api
$routes->resource('api/users', ['controller' => 'Api\UserController']);
$routes->resource('api/paket', ['controller' => 'Api\PaketController']);
$routes->resource('api/about', ['controller' => 'Api\AboutController']);
$routes->resource('api/jamaah', ['controller' => 'Api\JamaahController']);
$routes->resource('api/payment', ['controller' => 'Api\PaymentController']);
$routes->resource('api/cicilan', ['controller' => 'Api\CicilanController']);
$routes->resource('api/pegawai', ['controller' => 'Api\PegawaiController']);
$routes->resource('api/slipgaji', ['controller' => 'Api\SlipGajiController']);
$routes->resource('api/potongan', ['controller' => 'Api\PotonganController']);
// update method
$routes->post('api/users/(:num)', 'Api\UserController::update/$1'); //update data users
$routes->post('api/paket/(:num)', 'Api\PaketController::update/$1'); //update data paket haji umroh
$routes->post('api/about/(:num)', 'Api\AboutController::update/$1'); //update data perusahaan
$routes->post('api/jamaah/(:num)', 'Api\JamaahController::update/$1'); //update data jamaah
$routes->post('upass/(:num)', 'Api\UserController::uppass/$1'); //ubah password in profile page
$routes->post('api/cicilan/(:num)', 'Api\CicilanController::update/$1'); //update data cicilan
$routes->post('api/pegawai/(:num)', 'Api\PegawaiController::update/$1'); //update data pegawai
$routes->post('api/pegawais/(:num)', 'Api\PegawaiController::updatestatus/$1'); //update status pegawai 0
$routes->post('api/slipgaji/(:num)', 'Api\SlipGajiController::update/$1'); //update data slip gaji
$routes->post('api/potongan/(:num)', 'Api\PotonganController::update/$1'); //update data potongan

// get where user status 1
$routes->get('api/usersw', 'Api\UserController::indexw');
$routes->get('api/usersw0', 'Api\UserController::indexw0');
$routes->get('api/pegawai0', 'Api\PegawaiController::index0');
$routes->get('api/slipgaji2/(:num)', 'Api\SlipGajiController::index2/$1');
$routes->get('api/potongan2/(:num)', 'Api\PotonganController::index2/$1');
