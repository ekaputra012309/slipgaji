<?php

namespace App\Models;

use CodeIgniter\Model;

class PegawaiModel extends Model
{
    protected $table      = 'tbl_pegawai';
    protected $primaryKey = 'id';
    protected $allowedFields = ['nip', 'nama_pegawai', 'no_rek', 'create_at', 'status'];
    protected $useAutoIncrement = true;
    protected $dateFormat = 'date';
    protected $createdField = 'create_at';
}
