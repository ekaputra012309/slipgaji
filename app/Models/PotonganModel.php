<?php

namespace App\Models;

use CodeIgniter\Model;

class PotonganModel extends Model
{
    protected $table = 'tbl_potongan';
    protected $primaryKey = 'id_potongan';
    protected $allowedFields = [
        'id_potongan', 'id_pegawai', 'gaji', 'ikahi_cab', 'lain2',
        'arisan_gabungan', 'simpan_pinjam', 'iuran_dyk', 'iuran_koperasi',
        'ptwp', 'ipaspi', 'pinjaman_koperasi', 'bapor', 'kebersamaan_hakim',
        'mushola', 'bri_bsm_jabar', 'sewa_rumah', 'iuran_hakim', 'create_at'
    ];
    protected $useAutoIncrement = true;
    protected $dateFormat = 'date';
    protected $createdField = 'create_at';
}
