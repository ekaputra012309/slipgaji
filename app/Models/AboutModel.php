<?php

namespace App\Models;

use CodeIgniter\Model;

class AboutModel extends Model
{
    protected $table      = 'tbl_about';
    protected $primaryKey = 'id';
    protected $allowedFields = ['logo', 'nama', 'alias', 'alamat', 'deskripsi', 'created_at'];
    protected $useAutoIncrement = true;
}
