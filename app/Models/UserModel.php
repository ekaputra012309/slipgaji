<?php

namespace App\Models;

use CodeIgniter\Model;

class UserModel extends Model
{
    protected $table      = 'tbl_users';
    protected $primaryKey = 'id';
    protected $allowedFields = ['username', 'fullname', 'email', 'pass', 'phone', 'profile_image', 'created_at', 'status'];
    protected $useAutoIncrement = true;
}
