<?php

namespace App\Models;

use CodeIgniter\Model;

class SlipGajiModel extends Model
{
    protected $table = 'tbl_slipgaji';
    protected $primaryKey = 'id';
    protected $allowedFields = [
        'id', 'id_pegawai', 'gaji_bulan', 'gaji', 'ikahi_cab', 'lain2',
        'arisan_gabungan', 'simpan_pinjam', 'iuran_dyk', 'iuran_koperasi',
        'ptwp', 'ipaspi', 'pinjaman_koperasi', 'bapor', 'kebersamaan_hakim',
        'mushola', 'bri_bsm_jabar', 'sewa_rumah', 'iuran_hakim', 'create_at'
    ];
    protected $useAutoIncrement = true;
    protected $dateFormat = 'date';
    protected $createdField = 'create_at';
}
