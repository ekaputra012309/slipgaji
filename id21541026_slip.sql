-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Nov 17, 2023 at 02:39 AM
-- Server version: 10.5.20-MariaDB
-- PHP Version: 7.3.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `id21541026_slip`
--

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `version` varchar(255) NOT NULL,
  `class` varchar(255) NOT NULL,
  `group` varchar(255) NOT NULL,
  `namespace` varchar(255) NOT NULL,
  `time` int(11) NOT NULL,
  `batch` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `version`, `class`, `group`, `namespace`, `time`, `batch`) VALUES
(1, '2023-07-28-120748', 'App\\Database\\Migrations\\CreateTblPaket', 'default', 'App', 1693200378, 1),
(2, '2023-07-28-120954', 'App\\Database\\Migrations\\CreateTblUsers', 'default', 'App', 1693200378, 1),
(3, '2023-07-28-121214', 'App\\Database\\Migrations\\CreateTblAbout', 'default', 'App', 1693200378, 1),
(4, '2023-07-28-121446', 'App\\Database\\Migrations\\CreateJamaahTable', 'default', 'App', 1693200378, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_about`
--

CREATE TABLE `tbl_about` (
  `id` int(5) UNSIGNED NOT NULL,
  `logo` varchar(255) DEFAULT NULL,
  `nama` varchar(100) NOT NULL,
  `alias` varchar(50) DEFAULT NULL,
  `alamat` text DEFAULT NULL,
  `deskripsi` text DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

--
-- Dumping data for table `tbl_about`
--

INSERT INTO `tbl_about` (`id`, `logo`, `nama`, `alias`, `alamat`, `deskripsi`, `created_at`) VALUES
(1, 'uploads/1699942832_eb0603c033946b3f1b34.png', 'Pengadilan Tinggi DKI Jakarta', 'PTDJ', 'Jl. Letnan Jendral Suprapto, RT.9/RW.7, Cemp. Putih Tim., Kec. Cemp. Putih, Kota Jakarta Pusat, Daerah Khusus Ibukota Jakarta 10510', '<div>Tlp &amp; Fax&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: 021-4252069 atau 021-4254257</div><div>WhatsApp Informasi &nbsp;&nbsp;&nbsp;&nbsp;: 081286982789</div><div>WhatsApp Pengaduan : 085161655415</div><div>E-mail &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;info.ptdkijakarta@mail.com</div>', '2023-08-30 07:34:28');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pegawai`
--

CREATE TABLE `tbl_pegawai` (
  `id` int(11) NOT NULL,
  `nip` text NOT NULL,
  `nama_pegawai` varchar(255) NOT NULL,
  `no_rek` text NOT NULL,
  `create_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `tbl_pegawai`
--

INSERT INTO `tbl_pegawai` (`id`, `nip`, `nama_pegawai`, `no_rek`, `create_at`) VALUES
(1, '040049638000000000', 'YONISMAN, SH., MH', '008201044292502', '2023-11-09 05:55:38'),
(2, '195701311985031003', 'SINGGIH BUDI PRAKOSO', '033201158027505', '2023-11-09 05:56:32'),
(3, '195703141985031002', 'CHRISNO RAMPALODJI, SH., MH', '038701000454560', '2023-11-09 05:57:02'),
(4, '195703171986121001', 'DR. H. SUPRAPTO, SH., M.HUM', '201001000226560', '2023-11-09 07:50:33'),
(5, '195704011985031001', 'TJOKORDA RAI SUAMBA, SH', '016001013636509', '2023-11-10 06:22:17'),
(6, '195705021983031002', 'EWIT SOETRIADI, SH., MH', '002001064190506', '2023-11-14 06:15:42'),
(7, '195705021984031001', 'MULYANTO, SH., MH', '081601002434503', '2023-11-16 02:50:22'),
(8, '195707171984032007', 'INDAH SULISTYOWATI, SH', '001201092831503', '2023-11-16 02:50:22'),
(9, '195707251986121001', 'TONY PRIBADI, SH., MH', '033201158021509', '2023-11-16 02:50:22'),
(10, '195708171981031041', 'ABDUL FATTAH, SH., MH', '033601076049506', '2023-11-16 02:50:22'),
(11, '195710091985032003', 'ANNA ANDANAWARIH, SH., M.HUM', '116701008003504', '2023-11-16 02:50:22'),
(12, '195712121984031004', 'BERLIN DAMANIK, SH., M.HUM', '033201158096504', '2023-11-16 02:50:23'),
(13, '195805201983031006', 'HARIS MUNANDAR, SH', '041701000973503', '2023-11-16 02:50:23'),
(14, '195805251984031005', 'GUNAWAN GUSMO, SH., M.HUM', '061001007356506', '2023-11-16 02:50:23'),
(15, '195806051985031004', 'H. YAHYA SYAM, SH., MH', '203901009156502', '2023-11-16 02:50:23'),
(16, '195806071984031001', 'DR. BINSAR GULTOM, SH., SE., MH', '005901077728505', '2023-11-16 02:50:23'),
(17, '195808141983021002', 'H. AGUSTI, SH., M.HUM', '007501113430507', '2023-11-16 02:50:23'),
(18, '195809151982031004', 'SUGENG RIYONO, SH., M.HUM', '307101001710503', '2023-11-16 02:50:23'),
(19, '195811121986122001', 'HANDRI ANIK EFFENDI, SH', '118201000078569', '2023-11-16 02:50:23'),
(20, '195811231985121001', 'NELSON PASARIBU, SH., MH', '033201158001509', '2023-11-16 02:50:23'),
(21, '195901011985031001', 'H. KHAIRUL FUAD, SH., M.HUM', '130201000118507', '2023-11-16 02:50:23'),
(22, '195901111986121001', 'H. TEGUH HARIANTO, SH., M.HUM', '033501023147505', '2023-11-16 02:50:23'),
(23, '195901121986122001', 'HJ. RITA KOMALA, SH', '096601033218533', '2023-11-16 02:58:36'),
(24, '195902141985031004', 'IDA BAGUS DWIYANTARA, SH., M.HUM', '113001000512502', '2023-11-16 03:35:14'),
(25, '195904071985031005', 'KAREL TUPPU, SH., MH', '041601000121568', '2023-11-16 03:37:20'),
(26, '195908131989031005', 'R. IIM NUROHIM, SH', '081301003646509', '2023-11-16 04:03:05'),
(27, '195908261985031004', 'H. MOHAMMAD LUTFI, SH., MH', '143601000023569', '2023-11-16 04:04:51'),
(28, '195909041984031004', 'DR. H. HERRI SWANTORO, SH., MH', '039001010416507', '2023-11-16 04:06:31'),
(29, '195909081986121001', 'SUMPENO, SH., MH', '118201000090561', '2023-11-16 04:11:49'),
(30, '195909121985122001', 'FAHIMAH BASYIR, SH', '211401003462509', '2023-11-16 04:14:01'),
(31, '195911301985031001', 'H. ERWAN MUNAWAR, SH., MH', '000301000437564', '2023-11-16 04:15:41'),
(32, '196003101985121001', 'PONTAS EFENDI, SH', '026601018369507', '2023-11-16 04:18:02'),
(33, '196004011985032002', 'ISTININGSIH RAHAYU, SH', '004501031345502', '2023-11-16 04:20:14'),
(34, '196005031985031005', 'SUGENG HIYANTO, SH., MH', '061001006019509', '2023-11-16 04:22:22'),
(35, '196007271986021001', 'H. YULMAN, SH., MH', '322201006148536', '2023-11-16 04:25:04'),
(36, '196009041988031005', 'SUTARTO, SH., M.HUM', '005501055679504', '2023-11-16 04:32:24'),
(37, '196009111985032002', 'MULTINING DYAH ELY MARIANI, SH., M.HUM', '038501000021563', '2023-11-16 04:35:49'),
(38, '196011031985122001', 'BUDI HAPSARI, SH', '091201000909504', '2023-11-16 04:38:20'),
(39, '196101151985121001', 'SUBACHRAN HARDI MULYONO, SH., MH', '024701029174503', '2023-11-16 04:40:42'),
(40, '196208081986121001', 'TOROWA DAELI, SH., MH', '111501001946504', '2023-11-16 04:48:18'),
(41, '196210291986121001', 'H. CAKRA ALAM, SH., MH', '011101034630501', '2023-11-16 04:51:21'),
(42, '195801261988032001', 'HERSLILY MOKOGINTA, SH', '012001048414506', '2023-11-16 04:56:19'),
(43, '196411101987021001', 'TAVIP DWIYATMIKO, SH., MH', '033201158215506', '2023-11-16 04:57:44'),
(44, '196707031988031004', 'BURHANUDDIN, SH', '022501032875500', '2023-11-16 05:06:09'),
(45, '196911301992031001', 'SUDIYANTO, SH., MH', '033201158103505', '2023-11-16 06:54:09'),
(46, '196112231984032001', 'DRA. ENDANG PRIMANAH NURPUJIATI, BC.IP,SH.,MH', '033201157897509', '2023-11-16 08:37:25'),
(47, '196206131989121003', 'L. R. SOPHAN GIRSANG, SH., MH', '156201000545504', '2023-11-16 08:38:39'),
(48, '196202081988032001', 'ROMA SIALLAGAN, SH', '033201158044507', '2023-11-16 08:39:56'),
(49, '196207221981032001', 'LISNUR FAUZIAH, SH', '322201006064538', '2023-11-16 08:41:08'),
(50, '196207251984121001', 'JAMSON SIRINGO RINGO, SH., MH', '033201157999505', '2023-11-16 08:42:41'),
(51, '196209121988032006', 'NURUSSABIHA, SH', '039901037147509', '2023-11-16 08:43:32'),
(52, '196210101985031009', 'SUYATNO, SH., MH', '113001000371508', '2023-11-16 08:44:19'),
(53, '196302031982032001', 'BETTY HARTATI, SH', '033201158003501', '2023-11-16 08:45:44'),
(54, '196302081983032001', 'INNA ISKANTRIANA, SH., MH', '010501031779503', '2023-11-16 08:46:59'),
(55, '196303271985032002', 'WATTY WIARTI, SH', '032901063797509', '2023-11-16 08:48:12'),
(56, '196308221989032001', 'HJ. NOERHAYATI, SH', '033201158004507', '2023-11-16 08:49:02'),
(57, '196407291986031005', 'BUDI SANTOSO, SH', '010001034977508', '2023-11-16 08:49:56'),
(58, '196410141983111001', 'BAMBANG SIRAJUDDIN, SH., MH', '053801014126502', '2023-11-16 08:50:56'),
(59, '196412191986011001', 'ISARAEL SITUMEANG, SH.,MH', '033201158002505', '2023-11-16 08:52:00'),
(60, '196412251991032002', 'RATNA SUMINAR, SH., MH', '182201003889535', '2023-11-16 08:57:01'),
(61, '196602121986031003', 'H. SUMIR, SH.,MH', '033201157912503', '2023-11-16 08:58:12'),
(62, '196607101986032002', 'NANIK WINARSIH, SH. MH', '322201002756507', '2023-11-16 08:59:03'),
(63, '196302131989031003', 'MOHAMMAD NAJIB, SH., MH', '113001000428509', '2023-11-16 09:00:03');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_slipgaji`
--

CREATE TABLE `tbl_slipgaji` (
  `id` int(11) NOT NULL,
  `id_pegawai` int(11) NOT NULL,
  `gaji_bulan` date NOT NULL,
  `gaji` int(11) NOT NULL,
  `ikahi_cab` int(11) NOT NULL,
  `lain2` int(11) NOT NULL,
  `arisan_gabungan` int(11) NOT NULL,
  `simpan_pinjam` int(11) NOT NULL,
  `iuran_dyk` int(11) NOT NULL,
  `iuran_koperasi` int(11) NOT NULL,
  `ptwp` int(11) NOT NULL,
  `ipaspi` int(11) NOT NULL,
  `pinjaman_koperasi` int(11) NOT NULL,
  `bapor` int(11) NOT NULL,
  `kebersamaan_hakim` int(11) NOT NULL,
  `mushola` int(11) NOT NULL,
  `bri_bsm_jabar` int(11) NOT NULL,
  `sewa_rumah` int(11) NOT NULL,
  `iuran_hakim` int(11) NOT NULL,
  `create_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_users`
--

CREATE TABLE `tbl_users` (
  `id` int(5) UNSIGNED NOT NULL,
  `username` varchar(50) NOT NULL,
  `fullname` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `pass` varchar(255) NOT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `profile_image` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT current_timestamp(),
  `status` enum('0','1') NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

--
-- Dumping data for table `tbl_users`
--

INSERT INTO `tbl_users` (`id`, `username`, `fullname`, `email`, `pass`, `phone`, `profile_image`, `created_at`, `status`) VALUES
(2, 'admin', 'admin ptdj', 'admin@gmail.com', '$2y$10$24JqH94a.5dw3yu2UxwloeHiBzoPIH0YRr.oerCxTq4UdM1RSA27W', '089999888999', 'uploads/1693715896_fd9eb6051acf6f1ecf4d.jpg', '2023-08-30 06:41:06', '1'),
(58, 'YONISMAN, SH., MH', 'YONISMAN, SH., MH', '040049638000000000', '$2y$10$LR47HFLGOyydLbSkT1XL0u7ndIty4m6c8NoLvVMWE4wxuFfVjefPO', '', NULL, '2023-11-17 02:22:07', '0'),
(59, 'SINGGIH BUDI PRAKOSO', 'SINGGIH BUDI PRAKOSO', '195701311985031003', '$2y$10$/ovLMP8NBnKe3r9n4DcRzOteOvGh3HUcQ.DV.FVUwy.uo2q4KN7W6', '', NULL, '2023-11-17 02:22:07', '0'),
(60, 'CHRISNO RAMPALODJI, SH., MH', 'CHRISNO RAMPALODJI, SH., MH', '195703141985031002', '$2y$10$aUbk3DwHAXwpiEG.4uy9TeTkSjn5kt6y2tXh9Mo6svXhcvubTPDui', '', NULL, '2023-11-17 02:22:07', '0'),
(61, 'DR. H. SUPRAPTO, SH., M.HUM', 'DR. H. SUPRAPTO, SH., M.HUM', '195703171986121001', '$2y$10$Pwcxu81znf8y5KlU7yVn6.XqggMtZwRDOtofmtPTt0xi2s6wbvAU6', '', NULL, '2023-11-17 02:22:07', '0'),
(62, 'TJOKORDA RAI SUAMBA, SH', 'TJOKORDA RAI SUAMBA, SH', '195704011985031001', '$2y$10$3qj0bdQAnzZYFzWp3P7.ruGO1G5Gx3dwzfEuohj9yYJ.uxapmK9em', '', NULL, '2023-11-17 02:22:07', '0'),
(63, 'EWIT SOETRIADI, SH., MH', 'EWIT SOETRIADI, SH., MH', '195705021983031002', '$2y$10$Cl8YcbotcrLojZ3jz2klte26ofQAAIrtoI9K/niSdE.puOykmmRuq', '', NULL, '2023-11-17 02:22:07', '0'),
(64, 'MULYANTO, SH., MH', 'MULYANTO, SH., MH', '195705021984031001', '$2y$10$nAcozXa3z5ceNPMOzbeScO/i8I8AOu1YznvIFVjtSBdC.L3.dSJz.', '', NULL, '2023-11-17 02:22:07', '0'),
(65, 'INDAH SULISTYOWATI, SH', 'INDAH SULISTYOWATI, SH', '195707171984032007', '$2y$10$l5Eh3y.GXB4g88QMDsRU6ucBw1AlNsBpTFLHfwMcKf7rtN51O/RNC', '', NULL, '2023-11-17 02:22:07', '0'),
(66, 'TONY PRIBADI, SH., MH', 'TONY PRIBADI, SH., MH', '195707251986121001', '$2y$10$MRrh3v6Bsmzce2DKGnV4Z.DbwSZbnRskV7XAE2Yez/Xy3GpXm97BK', '', NULL, '2023-11-17 02:22:07', '0'),
(67, 'ABDUL FATTAH, SH., MH', 'ABDUL FATTAH, SH., MH', '195708171981031041', '$2y$10$XEeNkN3dFJ3OBqENww5EPex8aIokR/Cit8cyah1NXJNNuAGgR7OWO', '', NULL, '2023-11-17 02:22:07', '0'),
(68, 'ANNA ANDANAWARIH, SH., M.HUM', 'ANNA ANDANAWARIH, SH., M.HUM', '195710091985032003', '$2y$10$/6FrylRA5vP3h/jGqXAR1O5N8lu9TGdfb7WDRMk8m2PZis8cKrv1q', '', NULL, '2023-11-17 02:22:07', '0'),
(69, 'BERLIN DAMANIK, SH., M.HUM', 'BERLIN DAMANIK, SH., M.HUM', '195712121984031004', '$2y$10$GcvyfyCawaDxCuOa6bZb5.gSItWeZTcDdTIIVGCuQmC2JqhKerQAO', '', NULL, '2023-11-17 02:22:07', '0'),
(70, 'HARIS MUNANDAR, SH', 'HARIS MUNANDAR, SH', '195805201983031006', '$2y$10$f/J.coX1oq7EZpxtBH57cuC3khQi5vTpAsv2xJGh/dobRtMk4cAd2', '', NULL, '2023-11-17 02:22:07', '0'),
(71, 'GUNAWAN GUSMO, SH., M.HUM', 'GUNAWAN GUSMO, SH., M.HUM', '195805251984031005', '$2y$10$yMczMTCpxTdEc2w9pMLKWevMRA3V7sBB3UIIAN/p7D3LJs0g6dJTe', '', NULL, '2023-11-17 02:22:07', '0'),
(72, 'H. YAHYA SYAM, SH., MH', 'H. YAHYA SYAM, SH., MH', '195806051985031004', '$2y$10$G/njCRDXqLpxff3RZzE6Pef12G24HJgVAuDr1JZnjDk381vSQaWVi', '', NULL, '2023-11-17 02:22:07', '0'),
(73, 'DR. BINSAR GULTOM, SH., SE., MH', 'DR. BINSAR GULTOM, SH., SE., MH', '195806071984031001', '$2y$10$PNoq4y6wAXV.5gDoMALS3uMaOfn8vEnyXlU4hlWzwXOZ8iPHI6Uea', '', NULL, '2023-11-17 02:22:07', '0'),
(74, 'H. AGUSTI, SH., M.HUM', 'H. AGUSTI, SH., M.HUM', '195808141983021002', '$2y$10$BoB1YnptxSztT.ClXntmrOcJi7K7jLdu3kg5RgoBx7cLdS4zghCEG', '', NULL, '2023-11-17 02:22:07', '0'),
(75, 'SUGENG RIYONO, SH., M.HUM', 'SUGENG RIYONO, SH., M.HUM', '195809151982031004', '$2y$10$gaJ4hyU7Krm0fCZ6uXF8O.M6TeB0uXzwvNb6crHZCC8X14zUt.736', '', NULL, '2023-11-17 02:22:07', '0'),
(76, 'HANDRI ANIK EFFENDI, SH', 'HANDRI ANIK EFFENDI, SH', '195811121986122001', '$2y$10$m/O0Cv.d4hB25BfZqPqvSuvn/M3ETOgSHP2qL7zzXk3fj2oFw0aMa', '', NULL, '2023-11-17 02:22:07', '0'),
(77, 'NELSON PASARIBU, SH., MH', 'NELSON PASARIBU, SH., MH', '195811231985121001', '$2y$10$oXj6.FPSOe/fRVycpfFVceEh5YOfvJEguoSt6wBYcCYEZhPmB4Rxy', '', NULL, '2023-11-17 02:22:07', '0'),
(78, 'H. KHAIRUL FUAD, SH., M.HUM', 'H. KHAIRUL FUAD, SH., M.HUM', '195901011985031001', '$2y$10$pakRHAQaOJyJYmsFGOCFUeTYMPhPiToQrSEJ9YT1Z4HD0WR1vjH.K', '', NULL, '2023-11-17 02:22:07', '0'),
(79, 'H. TEGUH HARIANTO, SH., M.HUM', 'H. TEGUH HARIANTO, SH., M.HUM', '195901111986121001', '$2y$10$ijadSTJSLAPLlvnGH3cLe.bsucsJL9HY16AfkEkmj3xJkVS63Lm.e', '', NULL, '2023-11-17 02:22:07', '0'),
(80, 'HJ. RITA KOMALA, SH', 'HJ. RITA KOMALA, SH', '195901121986122001', '$2y$10$DGV9qeFYjS0EdZY83Se5MeaiBynbkjN9FxQFFkGC42WgPmCzY2DYq', '', NULL, '2023-11-17 02:22:07', '0'),
(81, 'IDA BAGUS DWIYANTARA, SH., M.HUM', 'IDA BAGUS DWIYANTARA, SH., M.HUM', '195902141985031004', '$2y$10$RKNEV8E08yRFbzBCnfqj9OFXJbpQWWYWfbR2b3aAxGXxYz/.5v3jG', '', NULL, '2023-11-17 02:22:07', '0'),
(82, 'KAREL TUPPU, SH., MH', 'KAREL TUPPU, SH., MH', '195904071985031005', '$2y$10$yRrrRnjwtgEQZPrvkHDyq.yqdIXXhDz3VvJbkJbsMglNFXb3EhF4.', '', NULL, '2023-11-17 02:22:07', '0'),
(83, 'R. IIM NUROHIM, SH', 'R. IIM NUROHIM, SH', '195908131989031005', '$2y$10$7RlujLNI3s8Zps/w17uYseGvxRTC4Dx2.07CgWUr1jhuXmpJxF4ra', '', NULL, '2023-11-17 02:22:07', '0'),
(84, 'H. MOHAMMAD LUTFI, SH., MH', 'H. MOHAMMAD LUTFI, SH., MH', '195908261985031004', '$2y$10$5uClOd5CZSHsaXz3tfPSYeE7R3yQceEScuF0gwOnKB0DOywQOv4l6', '', NULL, '2023-11-17 02:22:07', '0'),
(85, 'DR. H. HERRI SWANTORO, SH., MH', 'DR. H. HERRI SWANTORO, SH., MH', '195909041984031004', '$2y$10$b.ISYpjVC3wI0UiwUiZTYurRJxE0jzXoKmdoIS5K5lKEUFMIMsMae', '', NULL, '2023-11-17 02:22:07', '0'),
(86, 'SUMPENO, SH., MH', 'SUMPENO, SH., MH', '195909081986121001', '$2y$10$c9tUTmNr0.gqSU6mxaXy8.8VvuwuvbOjXagFQVxDMByitAIE7mWyG', '', NULL, '2023-11-17 02:22:07', '0'),
(87, 'FAHIMAH BASYIR, SH', 'FAHIMAH BASYIR, SH', '195909121985122001', '$2y$10$hGUnqHIc1wU5elwiuJvaU.CBNK//KvRFjQre54UTrWYprdMU2JXOa', '', NULL, '2023-11-17 02:22:07', '0'),
(88, 'H. ERWAN MUNAWAR, SH., MH', 'H. ERWAN MUNAWAR, SH., MH', '195911301985031001', '$2y$10$M/5EdGbyW7zeHfRYKwgPBOBN.qlRRB2qf.EEDTAvN9Fso36dHLlHG', '', NULL, '2023-11-17 02:22:07', '0'),
(89, 'PONTAS EFENDI, SH', 'PONTAS EFENDI, SH', '196003101985121001', '$2y$10$B9qvvRFBkc99EUo2OCww.Og8y1jdifUm5LTH2OB9I1f7sEIcf4I.O', '', NULL, '2023-11-17 02:22:07', '0'),
(90, 'ISTININGSIH RAHAYU, SH', 'ISTININGSIH RAHAYU, SH', '196004011985032002', '$2y$10$A5Yu9Hj09oLza8zZ0bu1aOyb8bqVdS4ZdIobATpxsK7bhKekvsq02', '', NULL, '2023-11-17 02:22:07', '0'),
(91, 'SUGENG HIYANTO, SH., MH', 'SUGENG HIYANTO, SH., MH', '196005031985031005', '$2y$10$ro2X7AZmH4LIwL54s1pAZOwzKW4YVfKiYwrU7yTktG0DAE90cMSV6', '', NULL, '2023-11-17 02:22:07', '0'),
(92, 'H. YULMAN, SH., MH', 'H. YULMAN, SH., MH', '196007271986021001', '$2y$10$eGNFlcAAkO4jNZ6RpwB3HeFzdh27mOXhX3qqSQ6mqW/yeXKwfsGR.', '', NULL, '2023-11-17 02:22:07', '0'),
(93, 'SUTARTO, SH., M.HUM', 'SUTARTO, SH., M.HUM', '196009041988031005', '$2y$10$9UIPWGJ0DOlYrQeYAXEoFOOgsGsWNHIaONuxBW41G.lGTOoRQBUkC', '', NULL, '2023-11-17 02:22:07', '0'),
(94, 'MULTINING DYAH ELY MARIANI, SH., M.HUM', 'MULTINING DYAH ELY MARIANI, SH., M.HUM', '196009111985032002', '$2y$10$xSuHdied1thG9Kppyw195el8cw2.CCUJDG7ohA/UOGVrVbqLizGiG', '', NULL, '2023-11-17 02:22:07', '0'),
(95, 'BUDI HAPSARI, SH', 'BUDI HAPSARI, SH', '196011031985122001', '$2y$10$gPZkHj5jnWbxSiviQaFgFOoekPyVmL/01gKJEMOR3vfqIQjoI4gx6', '', NULL, '2023-11-17 02:22:07', '0'),
(96, 'SUBACHRAN HARDI MULYONO, SH., MH', 'SUBACHRAN HARDI MULYONO, SH., MH', '196101151985121001', '$2y$10$/oYEJz2HUcbykYiEF6C1AusbnDCDenKGrB7OA4Zjve0Qvm5COV2UW', '', NULL, '2023-11-17 02:22:07', '0'),
(97, 'TOROWA DAELI, SH., MH', 'TOROWA DAELI, SH., MH', '196208081986121001', '$2y$10$71ihyG4RyMFBvCYm0ptleui7YTv1qMqjSv1TlLpiKtzf4A/ByDyjq', '', NULL, '2023-11-17 02:22:07', '0'),
(98, 'H. CAKRA ALAM, SH., MH', 'H. CAKRA ALAM, SH., MH', '196210291986121001', '$2y$10$Ab2w3fbG79LQu909Rsu9UuER0xUe7PCy9KKSOKXG5B90D3TQRGotC', '', NULL, '2023-11-17 02:22:07', '0'),
(99, 'HERSLILY MOKOGINTA, SH', 'HERSLILY MOKOGINTA, SH', '195801261988032001', '$2y$10$0M5lPAeKQu5QnNdbaQsgleBjHjIsSSELorv8EEX6MWmhgCmaFU.TW', '', NULL, '2023-11-17 02:22:07', '0'),
(100, 'TAVIP DWIYATMIKO, SH., MH', 'TAVIP DWIYATMIKO, SH., MH', '196411101987021001', '$2y$10$oO49m3xxBS4Ait6Qwy.4Qe7XCV8qy.PxtVYzFF/uWDz0ZfV/rerya', '', NULL, '2023-11-17 02:22:07', '0'),
(101, 'BURHANUDDIN, SH', 'BURHANUDDIN, SH', '196707031988031004', '$2y$10$4M4dftaVTyMaiiEH2GqXDeUU9jnopmyocblwxSFIQKo/9MuuJX1R6', '', NULL, '2023-11-17 02:22:07', '0'),
(102, 'SUDIYANTO, SH., MH', 'SUDIYANTO, SH., MH', '196911301992031001', '$2y$10$OYXfYfZbn9tH/jUBeU6n/e7wO1LjcoFiW39wurFaSF5TJne9TWBSG', '', NULL, '2023-11-17 02:22:07', '0'),
(103, 'DRA. ENDANG PRIMANAH NURPUJIATI, BC.IP,SH.,MH', 'DRA. ENDANG PRIMANAH NURPUJIATI, BC.IP,SH.,MH', '196112231984032001', '$2y$10$aXc1AMGjAv5riGVW3UqUqOhIJMzmg9zMWgLgODRmjTNMeyMrCoAn2', '', NULL, '2023-11-17 02:22:07', '0'),
(104, 'L. R. SOPHAN GIRSANG, SH., MH', 'L. R. SOPHAN GIRSANG, SH., MH', '196206131989121003', '$2y$10$/QRnc1jaSwutFwQBSjuUE.xIkcNKKCjy8VzDWQxTrYyejbNMlZ75y', '', NULL, '2023-11-17 02:22:07', '0'),
(105, 'ROMA SIALLAGAN, SH', 'ROMA SIALLAGAN, SH', '196202081988032001', '$2y$10$o8jASPMhTssRHHLgM9pwVOWygtgKR.NxIT6T1K.1uyFA2b9vR.Vxi', '', NULL, '2023-11-17 02:22:07', '0'),
(106, 'LISNUR FAUZIAH, SH', 'LISNUR FAUZIAH, SH', '196207221981032001', '$2y$10$U4eZH7JtEw03X8WwAU.jxuM4BT3/iy2fZFfbpRfTCB3ZlPxWAWW6y', '', NULL, '2023-11-17 02:22:07', '0'),
(107, 'JAMSON SIRINGO RINGO, SH., MH', 'JAMSON SIRINGO RINGO, SH., MH', '196207251984121001', '$2y$10$1ta04OwfLD3G7x5TLdoJjO0PjdApM9J1whmEVbeRZTRz8QDwaDp5y', '', NULL, '2023-11-17 02:22:07', '0'),
(108, 'NURUSSABIHA, SH', 'NURUSSABIHA, SH', '196209121988032006', '$2y$10$mGo5EWQdNxKNsA/XF/TSXefKVji3XeBJhLI4uaFPtx9S4sQH1etRa', '', NULL, '2023-11-17 02:22:07', '0'),
(109, 'SUYATNO, SH., MH', 'SUYATNO, SH., MH', '196210101985031009', '$2y$10$kbmuoV0V1jBWj1VmITNeleJLzuNThdiDMoaXIPimKe.FifBGXrdba', '', NULL, '2023-11-17 02:22:07', '0'),
(110, 'BETTY HARTATI, SH', 'BETTY HARTATI, SH', '196302031982032001', '$2y$10$QdegMkxbL1asaz6PHMjN3ukpRJNocMBM.TGgybZPiRxosgPCRR87q', '', NULL, '2023-11-17 02:22:07', '0'),
(111, 'INNA ISKANTRIANA, SH., MH', 'INNA ISKANTRIANA, SH., MH', '196302081983032001', '$2y$10$0hIO3Akm6Ig5987/E0AefeXpmZeqa5PfZUyg6RxGbM3Sav7HgUIWe', '', NULL, '2023-11-17 02:22:07', '0'),
(112, 'WATTY WIARTI, SH', 'WATTY WIARTI, SH', '196303271985032002', '$2y$10$oTWeyeQjK0tYhb9gkYp0.uNg43NqoVaYMeDwipm/MDJb6AUPilt1S', '', NULL, '2023-11-17 02:22:07', '0'),
(113, 'HJ. NOERHAYATI, SH', 'HJ. NOERHAYATI, SH', '196308221989032001', '$2y$10$47pRQXQM74WEDz.tgSzu4.DNrGoq.4jNE6Td8mst1uwOIqjrcJNCy', '', NULL, '2023-11-17 02:22:07', '0'),
(114, 'BUDI SANTOSO, SH', 'BUDI SANTOSO, SH', '196407291986031005', '$2y$10$yUfnCyEj67JxXB92BCZNke4k.vw7heDkYTOQyK5ECsqVS4WC.ZF66', '', NULL, '2023-11-17 02:22:07', '0'),
(115, 'BAMBANG SIRAJUDDIN, SH., MH', 'BAMBANG SIRAJUDDIN, SH., MH', '196410141983111001', '$2y$10$mixq35uoP3LWtRpJLcjGLOWEC6Ats3A4lnuE7UQxISxn1GzIM.31O', '', NULL, '2023-11-17 02:22:07', '0'),
(116, 'ISARAEL SITUMEANG, SH.,MH', 'ISARAEL SITUMEANG, SH.,MH', '196412191986011001', '$2y$10$ein1BWxthoQxiKhOACHbZeRPBVVrUwj6gOa7P7clCul6ly6.c3Iq2', '', NULL, '2023-11-17 02:22:07', '0'),
(117, 'RATNA SUMINAR, SH., MH', 'RATNA SUMINAR, SH., MH', '196412251991032002', '$2y$10$xngRu5Qh4ENtd6Z.NdrgAei7GWbvurRqmJ99qcu7ShsWcCSNsBhYm', '', NULL, '2023-11-17 02:22:07', '0'),
(118, 'H. SUMIR, SH.,MH', 'H. SUMIR, SH.,MH', '196602121986031003', '$2y$10$GxUMPrsSo8OiFZ3xs4KqX.GloADyO7N90M4NWO6VL1cABVu.uPf.u', '', NULL, '2023-11-17 02:22:07', '0'),
(119, 'NANIK WINARSIH, SH. MH', 'NANIK WINARSIH, SH. MH', '196607101986032002', '$2y$10$zQMZHK5WKTab5jwuUFYnnOXfLDdEoIz78dRX4YvT.ag8hI7S8SjvS', '', NULL, '2023-11-17 02:22:07', '0'),
(120, 'MOHAMMAD NAJIB, SH., MH', 'MOHAMMAD NAJIB, SH., MH', '196302131989031003', '$2y$10$/RbrnVWLBOnn3YkEVuxC6uRcs57NCIcINS0Pb3SJy1f8W.BGmriRu', '', NULL, '2023-11-17 02:22:07', '0');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_about`
--
ALTER TABLE `tbl_about`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_pegawai`
--
ALTER TABLE `tbl_pegawai`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_slipgaji`
--
ALTER TABLE `tbl_slipgaji`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_users`
--
ALTER TABLE `tbl_users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tbl_about`
--
ALTER TABLE `tbl_about`
  MODIFY `id` int(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tbl_pegawai`
--
ALTER TABLE `tbl_pegawai`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=64;

--
-- AUTO_INCREMENT for table `tbl_slipgaji`
--
ALTER TABLE `tbl_slipgaji`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_users`
--
ALTER TABLE `tbl_users`
  MODIFY `id` int(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=121;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
